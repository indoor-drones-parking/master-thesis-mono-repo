'use server';
import { revalidatePath } from 'next/cache';
import { ParkingSpotsService } from '../firestore/services/ParkingSpotsService';
import { ParkingSpot } from '../models/ParkingSpot';

export const updateParkingSpot = async (
  floorId: string,
  parkingSpot: ParkingSpot
) => {
  await ParkingSpotsService.updateParkingSpot(floorId, parkingSpot);
  revalidatePath(`/parkingoverview/${floorId}`);
  return true;
};
