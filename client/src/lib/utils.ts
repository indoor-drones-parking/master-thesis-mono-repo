import { type ClassValue, clsx } from 'clsx';
import { twMerge } from 'tailwind-merge';

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}
export function formatDateToString(date: string) {
  const daysOfWeek = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];
  const monthsOfYear = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  const newDate: Date = new Date(date);

  const dayOfWeek = daysOfWeek[newDate.getDay()];
  const dayOfMonth = ('0' + newDate.getDate()).slice(-2);
  const month = monthsOfYear[newDate.getMonth()];
  const year = newDate.getFullYear();
  const hours = ('0' + newDate.getHours()).slice(-2);
  const minutes = ('0' + newDate.getMinutes()).slice(-2);

  return `${dayOfWeek} ${dayOfMonth}.${month}-${year} at ${hours}:${minutes}`;
}
