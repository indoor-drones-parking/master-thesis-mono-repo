'use server';

import { ref, remove, set } from 'firebase/database';
import { redirect } from 'next/navigation';
import { database } from './firestore/config';

export async function handleDelete(id: string): Promise<boolean> {
  const deviationRef = ref(database, `deviations/${id}`);

  try {
    await remove(deviationRef);
    return true;
  } catch (err) {
    return false;
  }
}

export async function handleUpdateMark(id: string): Promise<boolean> {
  const fixedRef = ref(database, `deviations/${id}/fixed`);

  try {
    await set(fixedRef, true);
    return true;
  } catch (err) {
    return false;
  }
}
