import * as Minio from 'minio';

const minioConfig: any = {
  endPoint: process.env.MINIO_ENDPOINT,
  port: Number(process.env.MINIO_PORT),
  useSSL: false,
  accessKey: process.env.MINIO_ACCESS_KEY,
  secretKey: process.env.MINIO_SECRET_KEY,
};

const minioClient = new Minio.Client(minioConfig);

// Function to retrieve image from MinIO bucket
export async function getImageFromBucket(bucketName: string, objectKey: any) {
  try {
    const data: Minio.BucketItem[] = [];
    const stream = minioClient.listObjects('deviation-images', '', true);
    stream.on('data', function (obj) {
      data.push(obj);
    });
    stream.on('end', function (obj: any) {
      console.log(data);
    });
    stream.on('error', function (err) {
      console.log(err);
    });
    /*     const buckets = await minioClient;
    console.log('Success', buckets); */
  } catch (err: any) {
    console.log(err.message);
  }
}

export async function fetchImagesFromMinio(): Promise<Minio.BucketItem[]> {
  const data: Minio.BucketItem[] = [];
  const stream = minioClient.listObjects('deviaton-images', '', true);

  return new Promise<Minio.BucketItem[]>((resolve, reject) => {
    stream.on('data', (obj: Minio.BucketItem) => {
      data.push(obj);
    });

    stream.on('end', () => {
      resolve(data);
    });

    stream.on('error', (err) => {
      reject(err);
    });
  });
}

export const getImageSrc = async (name: string): Promise<string> => {
  return new Promise<string>(async (resolve, reject) => {
    const chunks: Uint8Array[] = [];
    const dataStream = await minioClient.getObject(
      'deviation-images',
      `${name}`
    );

    dataStream.on('data', (chunk: Uint8Array) => {
      chunks.push(chunk);
    });

    dataStream.on('end', () => {
      const imageData = Buffer.concat(chunks).toString('base64');
      resolve(`data:image/jpeg;base64,${imageData}`);
    });

    dataStream.on('error', (error: Error) => {
      reject(error);
    });
  });
};

export const getImageSrcForParkingSpot = async (
  name: string
): Promise<string> => {
  return new Promise<string>(async (resolve, reject) => {
    const chunks: Uint8Array[] = [];
    const dataStream = await minioClient.getObject(
      'mapping-images',
      `${name}.jpg`
    );

    dataStream.on('data', (chunk: Uint8Array) => {
      chunks.push(chunk);
    });

    dataStream.on('end', () => {
      const imageData = Buffer.concat(chunks).toString('base64');
      resolve(`data:image/jpeg;base64,${imageData}`);
    });

    dataStream.on('error', (error: Error) => {
      reject(error);
    });
  });
};
