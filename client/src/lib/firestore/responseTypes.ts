export type ServiceSuccessResponse<T> = {
  ok: true;
  data: T;
};

export type ServiceErrorResponse = {
  ok: false;
  error: ServiceErrorType;
};

export type ServiceResponse<T = unknown> =
  | ServiceSuccessResponse<T>
  | ServiceErrorResponse;

export type ServiceErrorType = {
  status?: number;
  message: string;
};
