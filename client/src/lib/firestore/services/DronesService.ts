import { Drone } from '@/lib/models/Drone';
import { ref, child, get } from 'firebase/database';
import { database } from '../config';

const getDrones = async (): Promise<Drone[]> => {
  const dbRef = ref(database);
  const response = await get(child(dbRef, '/drones'));
  const dronesObject = response.val();
  return Object.values(dronesObject);
};

export const DronesService = { getDrones };
