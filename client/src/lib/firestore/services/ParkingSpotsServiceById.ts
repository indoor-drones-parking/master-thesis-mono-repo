'use client';
import { ref, child, get } from 'firebase/database';
import { database } from '../config';
import { ParkingSpot } from '@/lib/models/ParkingSpot';

const getParkingSpotsByIds = async (
  ids: string[],
  parkingFloorId: String
): Promise<ParkingSpot[]> => {
  const dbRef = ref(database);
  // Use Promise.all() with Array.map() to await multiple asynchronous operations
  const responses = await Promise.all(
    ids.map(async (id) => {
      const snapshot = await get(
        child(dbRef, `/parkingFloors/${parkingFloorId}/${id}`)
      );
      return snapshot.val() as ParkingSpot;
    })
  );

  return responses.filter((spot) => spot !== null); // Filter out null values if any
};

export const ParkingSpotsServiceById = { getParkingSpotsByIds };
