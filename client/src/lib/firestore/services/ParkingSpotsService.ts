import { ref, child, get, onValue, update } from 'firebase/database';
import { database } from '../config';
import { ParkingSpot } from '@/lib/models/ParkingSpot';

const getParkingSpots = async (id: string): Promise<ParkingSpot[]> => {
  const dbRef = ref(database);
  const response = await get(child(dbRef, `/parkingFloors/${id}`));
  const val = Object.values(response.val()) as ParkingSpot[];
  return [
    ...val.sort((spot1, spot2) => {
      return spot1.number - spot2.number;
    }),
  ];
};

const updateParkingSpot = async (floorId: string, parkingSpot: ParkingSpot) => {
  const path = `/parkingFloors/${floorId}/${parkingSpot.id}`;
  const dbRef = ref(database, path);
  await update(dbRef, { isOccupied: parkingSpot.isOccupied });
};

const getParkingSpotsById = async (
  parkingFloorId: String,
  id: string
): Promise<ParkingSpot> => {
  const dbRef = ref(database);
  // Use Promise.all() with Array.map() to await multiple asynchronous operations

  const snapshot = await get(
    child(dbRef, `/parkingFloors/${parkingFloorId}/${id}`)
  );
  return snapshot.val() as ParkingSpot;
};

export const ParkingSpotsService = {
  getParkingSpots,
  updateParkingSpot,
  getParkingSpotsById,
};
