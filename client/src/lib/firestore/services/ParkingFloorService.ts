import { ref, child, get, onValue } from 'firebase/database';
import { database } from '../config';

import { ParkingFloors } from '@/lib/models/ParkigFloors';

const getParkingFloors = async (): Promise<ParkingFloors> => {
  const dbRef = ref(database);
  const response = await get(child(dbRef, `/parkingFloors`));
  return response.val() as ParkingFloors;
};

export const ParkingFloorsService = { getParkingFloors };
