'use client';
import { ref, child, get, set } from 'firebase/database';
import { database } from '../config';
import { Drone } from '@/lib/models/Drone';

const getDronesById = async (ids: string[]): Promise<Drone[]> => {
  const dbRef = ref(database);
  // Use Promise.all() with Array.map() to await multiple asynchronous operations
  const responses = await Promise.all(
    ids.map(async (id) => {
      const snapshot = await get(child(dbRef, `/drones/${id}`));
      console.log(id);
      console.log(snapshot.val());
      return snapshot.val() as Drone;
    })
  );

  return responses.filter((drone) => drone !== null); // Filter out null values if any
};

const updateDroneStatuses = async (ids: string[]): Promise<boolean> => {
  ids.forEach((id) => {
    try {
      const operationRef = ref(database, `/drones/${id}/operation`);
      const statusRef = ref(database, `/drones/${id}/status`);
      const droneMissionIdRef = ref(database, `drones/${id}/missionId`);

      set(operationRef, 'NONE');
      set(statusRef, 'IDLE');
      set(droneMissionIdRef, null);
    } catch (err) {
      return false;
    }
  });

  return true;
};

export const DroneService = { getDronesById, updateDroneStatuses };
