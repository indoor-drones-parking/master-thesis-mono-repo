import { ref, child, get } from 'firebase/database';
import { database } from '../config';
import { Mission } from '@/lib/models/Mission';

const getMissions = async (): Promise<Mission[]> => {
  const dbRef = ref(database);
  const response = await get(child(dbRef, '/missions'));
  const missionsObject = response.val();
  if (missionsObject) {
    return Object.values(missionsObject);
  }
  return [];
};

const getMissionById = async (id: string): Promise<Mission | undefined> => {
  const dbRef = ref(database);
  try {
    const response = await get(child(dbRef, `/missions/${id}`));
    const missionObject = response.val();
    if (missionObject) {
      return missionObject;
    }
  } catch (error) {
    return undefined;
  }
};

export const MissionService = { getMissions, getMissionById };
