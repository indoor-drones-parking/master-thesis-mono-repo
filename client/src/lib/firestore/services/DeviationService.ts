import { Deviation } from '@/lib/models/Devation';
import { ref, child, get } from 'firebase/database';
import { database } from '../config';

const getDeviations = async (): Promise<Deviation[]> => {
  const dbRef = ref(database);
  const response = await get(child(dbRef, '/deviations'));
  const deviationsObject = response.val();
  return deviationsObject;
};

const getDeviation = async (id: string): Promise<Deviation> => {
  const dbRef = ref(database);
  const response = await get(child(dbRef, `/deviations/${id}`));
  const deviationsObject = response.val();
  return deviationsObject;
};

export const DeviationService = { getDeviations, getDeviation };
