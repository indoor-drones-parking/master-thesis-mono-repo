export type ParkingSpot = {
  id: string;
  number: number;
  isOccupied: boolean;
  freeTimeDurationInSeconds?: number;
  hasFreePeriod?: boolean;
  last_mapped?: string;
};
