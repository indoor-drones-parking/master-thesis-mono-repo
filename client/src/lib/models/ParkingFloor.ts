import { ParkingSpot } from './ParkingSpot';

export type ParkingFloor = {
  [id: string]: ParkingSpot;
};

export type ParkingFloorDto = {
  id: string;
  number: number;
  parkingSpots?: ParkingSpot[];
};
