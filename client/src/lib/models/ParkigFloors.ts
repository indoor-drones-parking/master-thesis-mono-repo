import { ParkingFloor } from './ParkingFloor';

export type ParkingFloors = {
  [parkingFloorID: string]: ParkingFloor; // Keyed by floorId, each value is a ParkingFloor
};
