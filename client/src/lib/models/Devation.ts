export type Deviation = {
  id: string;
  parkingNumber: number;
  description: string;
  image: string; //altid være der
  firstMappedImage?: string; //second image
  firstMappedTimestamp?: string; //secondtimestamp
  timestamp: string;  //altidd vøre der
  parkingId: string;
};
