export type Drone = {
  id: string;
  name: string;
  operation: DroneOperation;
  status: DroneStatus;
  batteryPercentage: number;
  parkingFloorId: string;
  wifiName?: string;
  serialNumber?: string;
};

export type DroneOperation = 'MAPPER' | 'GUIDER' | 'NONE';

type DroneStatus = 'IN MISSION' | 'ERROR' | 'IDLE';
