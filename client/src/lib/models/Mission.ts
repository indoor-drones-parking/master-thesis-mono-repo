import { DroneOperation } from './Drone';
import { ParkingFloor } from './ParkingFloor';

export interface Mission {
  status: MissionStatus;
  type: DroneOperation;
  droneIds: string[];
  parkingFloorId: string;
  timeStarted: string;
  id: string;
}
export interface GuiderMission extends Mission {
  // Additional properties or methods specific to GuiderMission
}

export interface MapperMission extends Mission {
  parkingSpotsIds: string[];
  mapperInterval: number;
  // Additional properties or methods specific to MapperMission
}

type MissionStatus = 'IDLE' | 'ACTIVE';
