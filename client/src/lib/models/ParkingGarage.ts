import { ParkingFloor } from './ParkingFloor';

export type ParkingGarage = {
  id: string;
  parkingFloors: ParkingFloor[];
};
