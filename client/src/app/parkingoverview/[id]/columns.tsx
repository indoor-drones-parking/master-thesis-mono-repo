'use client';

import { GridColDef } from '@mui/x-data-grid';
import React from 'react';

export type ParkingSpotTableElement = {
  number: number;
  isOccupied: React.ReactNode;
};

export const columns: GridColDef[] = [
  {
    field: 'number',
    headerName: 'Number',
    width: 130,
    type: 'number',
  },
  {
    editable: true,
    field: 'isOccupied',
    headerName: 'Occupied',
    width: 150,
    type: 'boolean',
  },
];
