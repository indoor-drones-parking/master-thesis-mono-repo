import React from 'react';

type Props = {
  children: React.ReactNode;
};

const ParkingSpotContainer = ({ children }: Props) => {
  return (
    <div className="container mx-auto px-4 py-4">
      <div className="grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
        {children}
      </div>
    </div>
  );
};

export default ParkingSpotContainer;
