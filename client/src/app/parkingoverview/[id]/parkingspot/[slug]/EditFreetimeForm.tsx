'use client';
import {
  showErrorNotification,
  showSuccessNotification,
} from '@/components/client/Notifications';
import { database } from '@/lib/firestore/config';
import { ParkingSpot } from '@/lib/models/ParkingSpot';
import { ref, set } from 'firebase/database';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/navigation';

interface Props {
  parkingSpot: ParkingSpot;
  floorId: string;
}

const EditFreetimeForm = ({ parkingSpot, floorId }: Props) => {
  const router = useRouter();
  const [hasFreePeriod, setHasFreePeriod] = useState<boolean>(
    parkingSpot.hasFreePeriod || false
  );
  const [initialHours, setInitialHours] = useState<number>(0);
  const [initialMinutes, setInitialMinutes] = useState<number>(0);
  const [hours, setHours] = useState<number>(0);
  const [minutes, setMinutes] = useState<number>(0);

  useEffect(() => {
    if (parkingSpot.freeTimeDurationInSeconds != null) {
      const totalSeconds = parkingSpot.freeTimeDurationInSeconds;
      const initialHrs = Math.floor(totalSeconds / 3600);
      const initialMins = Math.floor((totalSeconds % 3600) / 60);
      setInitialHours(initialHrs);
      setInitialMinutes(initialMins);
      setHours(initialHrs);
      setMinutes(initialMins);
    }
  }, [parkingSpot.freeTimeDurationInSeconds]);

  const handleUpdate = () => {
    const totalSeconds = hours * 3600 + minutes * 60;
    const parkingSpotTimeRef = ref(
      database,
      `parkingFloors/${floorId}/${parkingSpot.id}/freeTimeDurationInSeconds`
    );
    const parkingSpotTimeBooleanRef = ref(
      database,
      `parkingFloors/${floorId}/${parkingSpot.id}/hasFreePeriod`
    );
    try {
      if (totalSeconds === 0) {
        set(parkingSpotTimeBooleanRef, false);
        set(parkingSpotTimeRef, null);
        showSuccessNotification('Free Time Limit Updated');
        router.refresh();
      } else {
        set(parkingSpotTimeBooleanRef, true);
        set(parkingSpotTimeRef, totalSeconds);
        showSuccessNotification('Free Time Limit Updated');
        router.refresh();
      }
    } catch (err) {
      showErrorNotification('Error While Updating Free Time Limit');
    }
  };

  const isButtonDisabled = () => {
    const totalSeconds = hours * 3600 + minutes * 60;
    const initialTotalSeconds = initialHours * 3600 + initialMinutes * 60;
    if (
      totalSeconds != initialTotalSeconds &&
      totalSeconds === 0 &&
      initialTotalSeconds === 0
    ) {
      return false;
    }
    return totalSeconds === initialTotalSeconds;
  };

  return (
    <div className="flex flex-col gap-4">
      <p className="text-lg font-semibold">Update free time limit:</p>
      <div className="flex w-full flex-row justify-between gap-4">
        <div className="flex w-1/2 flex-col">
          <label className="text-lg">Hours:</label>
          <input
            type="number"
            value={hours}
            onChange={(e) =>
              setHours(Math.max(0, parseInt(e.target.value, 10) || 0))
            }
            onFocus={(e) => e.target.select()}
            className="focus:shadow-outline w-full appearance-none rounded border px-3 py-2 leading-tight text-gray-700 shadow focus:outline-none"
            min="0"
          />
        </div>
        <div className="flex w-1/2 flex-col">
          <label className="text-lg ">Minutes:</label>
          <input
            type="number"
            value={minutes}
            onChange={(e) =>
              setMinutes(
                Math.max(0, Math.min(59, parseInt(e.target.value, 10) || 0))
              )
            }
            onFocus={(e) => e.target.select()}
            className="focus:shadow-outline w-full appearance-none rounded border px-3 py-2 leading-tight text-gray-700 shadow focus:outline-none"
            min="0"
            max="59"
          />
        </div>
      </div>
      <button
        onClick={handleUpdate}
        disabled={isButtonDisabled()} // Disable the button based on the form validity
        className={`focus:shadow-outline w-full rounded bg-green-500 px-4 py-2 font-bold text-white hover:bg-green-700 focus:outline-none ${
          isButtonDisabled() ? 'cursor-not-allowed opacity-50' : ''
        }`}
      >
        Update
      </button>
    </div>
  );
};

export default EditFreetimeForm;
