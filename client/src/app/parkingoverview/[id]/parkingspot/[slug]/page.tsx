import { ParkingSpotsService } from '@/lib/firestore/services/ParkingSpotsService';
import { getImageSrcForParkingSpot } from '@/lib/minio/pictureService';
import { ParkingSpot } from '@/lib/models/ParkingSpot';
import Image from 'next/image';
import React from 'react';
import { formatDateToString } from '@/lib/utils';
import EditFreetimeForm from './EditFreetimeForm';

export default async function ParkingSpotPage({
  params,
}: {
  params: { id: string; slug: string };
}) {
  const parkingSpot: ParkingSpot =
    await ParkingSpotsService.getParkingSpotsById(params.id, params.slug);

  const imageSrc = await getImageSrcForParkingSpot(
    `${params.id}/${params.slug}`
  );

  const getTimePeriod = (seconds: number) => {
    const hours = Math.floor(seconds / 3600);
    const minutes = Math.floor((seconds % 3600) / 60);

    const hoursString = hours > 0 ? `${hours} hour${hours > 1 ? 's' : ''}` : '';
    const minutesString =
      minutes > 0 ? `${minutes} minute${minutes > 1 ? 's' : ''}` : '';

    if (hoursString && minutesString) {
      return `${hoursString} and ${minutesString}`;
    } else if (hoursString) {
      return hoursString;
    } else if (minutesString) {
      return minutesString;
    } else {
      return '0 minutes';
    }
  };

  return (
    <>
      {parkingSpot ? (
        <div className="flex flex-col gap-2">
          <div className="flex justify-between space-x-8 rounded-lg bg-white p-4 shadow-md">
            <h1 className="m-2 text-xl font-bold">
              Parking Spot Number {parkingSpot.number}
            </h1>
            {parkingSpot.isOccupied ? (
              <div className="flex items-center space-x-2">
                <span className="text-lg font-semibold">Occupied</span>
                <div className="h-4 w-4 rounded-full bg-red-500"></div>
              </div>
            ) : (
              <div className="flex items-center space-x-2">
                <span className="text-lg font-semibold">Available</span>
                <div className="h-4 w-4 rounded-full bg-green-500"></div>
              </div>
            )}
          </div>
          <div className="flex gap-2  space-x-8 rounded-lg bg-white p-4 shadow-md">
            <div className="flex w-1/2 flex-col justify-between">
              <div className="flex  flex-col gap-2">
                <p>
                  {' '}
                  <b className="text-lg font-semibold">Located At Floor: </b>
                  {params.id.charAt(params.id.length - 1)}
                </p>
                {parkingSpot.hasFreePeriod !== undefined &&
                parkingSpot.freeTimeDurationInSeconds ? (
                  <p>
                    <b className="text-lg font-semibold">Free Time Limit:</b>{' '}
                    {getTimePeriod(parkingSpot.freeTimeDurationInSeconds)}.
                  </p>
                ) : (
                  <p>
                    <b className="text-lg font-semibold">Free Time Limit:</b>{' '}
                    None
                  </p>
                )}
              </div>
              <EditFreetimeForm floorId={params.id} parkingSpot={parkingSpot} />
            </div>

            {/* right side div */}
            <div className="flex w-1/2 flex-col items-center justify-center gap-2 text-left">
              <p className="text-lg font-semibold">Last mapped image:</p>
              {imageSrc != '' && (
                <Image
                  src={imageSrc} // Replace with the actual source of the second image
                  alt="Image Display"
                  width={400} // Adjust width and height as needed
                  height={100}
                />
              )}
              {parkingSpot.last_mapped && (
                <p>
                  <b className="text-lg font-semibold">Timestamp: </b>
                  {formatDateToString(parkingSpot.last_mapped)}
                </p>
              )}
            </div>
          </div>
        </div>
      ) : (
        <p>No parking spot found!</p>
      )}
    </>
  );
}
