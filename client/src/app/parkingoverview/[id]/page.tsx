import Image from 'next/image';
import ParkingImage from '@/assets/images/ParkingGarageFloorPlan.png';
import React from 'react';
import { ParkingSpotsService } from '@/lib/firestore/services/ParkingSpotsService';
import { ParkingDatagrid } from './ParkingDatagrid';
import { ref, child, get, onValue } from 'firebase/database';
import FloorContainer from '@/components/server/Drones/FloorContainer';
import ParkingSpotCard from '@/components/server/Floor/ParkingSpotCard';
import ParkingSpotContainer from './ParkingSpotContainer';
import Button from '@/components/server/Buttons/Button';
import OpenMap from './OpenMap';

export default async function Page({ params }: { params: { id: string } }) {
  const parkingSpots = await ParkingSpotsService.getParkingSpots(params.id);

  if (!parkingSpots) {
    return <h1>Cannot Load ParkingSpots</h1>;
  }

  return (
    <div>
      <div className="flex justify-between space-x-8 rounded-lg bg-white p-4 shadow-md">
        <h1 className="m-2 text-xl font-bold">
          Parking Spots At Floor {params.id.charAt(params.id.length - 1)}
        </h1>
        <OpenMap />
        <div className="flex gap-8">
          <div className="flex items-center space-x-2">
            <span className="text-lg font-semibold">Occupied</span>
            <div className="h-4 w-4 rounded-full bg-red-500"></div>
          </div>

          <div className="flex items-center space-x-2">
            <span className="text-lg font-semibold">Available</span>
            <div className="h-4 w-4 rounded-full bg-green-500"></div>
          </div>
        </div>
      </div>
      <ParkingSpotContainer>
        {parkingSpots.map((parkingspot, idx) => {
          return (
            <div
              className="group relative transform duration-300 hover:translate-x-1 hover:cursor-pointer hover:shadow-xl"
              key={idx}
            >
              <ParkingSpotCard
                parkingspot={parkingspot}
                floorId={params.id}
              ></ParkingSpotCard>
              <div className="absolute bottom-full left-1/2 mb-2 w-max -translate-x-1/2 transform rounded bg-black px-2 py-1 text-xs text-white opacity-0 transition-opacity duration-300 group-hover:opacity-100">
                Click to see more info
              </div>
            </div>
          );
        })}
      </ParkingSpotContainer>
      {/*old design       <Image
        src={ParkingImage}
        alt={'Parking Overview'}
        className="w-[70%] self-center"
      ></Image>
      <ParkingDatagrid
        parkingSpots={parkingSpots}
        id={params.id}
      ></ParkingDatagrid> */}
    </div>
  );
}
