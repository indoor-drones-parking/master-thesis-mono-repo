'use client';
import React, { useState, useEffect } from 'react';
import { ref, onValue } from 'firebase/database';
import DataGrid from '@/components/server/Datagrid/Datagrid';
import { ParkingSpot } from '@/lib/models/ParkingSpot';
import { columns } from './columns';
import { database } from '@/lib/firestore/config';
import { updateParkingSpot } from '@/lib/actions/parkingSpot-actions';

type Props = {
  parkingSpots: ParkingSpot[];
  id: string;
};

export function ParkingDatagrid({ parkingSpots, id }: Props) {
  const [parkingSpotsLive, setParkingSpotsLive] =
    useState<ParkingSpot[]>(parkingSpots);

  useEffect(() => {
    const parkingSpotRef = ref(database, `/parkingFloors/${id}`);
    const unSub = onValue(parkingSpotRef, (snapchot) => {
      const values = Object.values(snapchot.val()) as ParkingSpot[];
      setParkingSpotsLive([
        ...values.sort((spot1, spot2) => {
          return spot1.number - spot2.number;
        }),
      ]);
    });
    return () => unSub();
  }, []);

  const onRowUpdate = async (updatedRow: ParkingSpot): Promise<boolean> => {
    await updateParkingSpot(id, updatedRow);
    return true;
  };

  return (
    <React.Fragment>
      <DataGrid
        data={parkingSpotsLive}
        columns={columns}
        onRowUpdate={onRowUpdate}
      ></DataGrid>
    </React.Fragment>
  );
}
