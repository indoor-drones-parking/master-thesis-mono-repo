'use client';
import React, { useState, useRef, useEffect } from 'react';
import Image from 'next/image';
import ParkingImage from '@/assets/images/ParkingGarageFloorPlan.png';

const OpenMap = () => {
  const [modalOpen, setModalOpen] = useState(false);
  const modalRef = useRef<HTMLDivElement>(null);

  const openModal = () => {
    setModalOpen(true);
  };

  const closeModal = () => {
    setModalOpen(false);
  };

  const handleOutsideClick = (event: { target: any }) => {
    if (modalRef.current && !modalRef.current.contains(event.target)) {
      closeModal();
    }
  };

  // Attach or detach event listener based on modal state
  useEffect(() => {
    if (modalOpen) {
      document.addEventListener('mousedown', handleOutsideClick);
    } else {
      document.removeEventListener('mousedown', handleOutsideClick);
    }
    // Cleanup function to remove event listener on component unmount
    return () => {
      document.removeEventListener('mousedown', handleOutsideClick);
    };
  }, [modalOpen]);

  return (
    <>
      <button
        className="m-2 rounded bg-blue-500 px-4 py-2 font-bold text-white hover:bg-blue-700"
        onClick={openModal}
      >
        View map
      </button>
      {modalOpen && (
        <div className="fixed left-0 top-0 z-50 flex h-full w-full items-center justify-center overflow-scroll bg-black bg-opacity-20">
          <div
            ref={modalRef}
            className="relative m-2 rounded-lg bg-white p-8"
            style={{
              maxWidth: 'calc(100% - 40px)',
              maxHeight: 'calc(100% - 40px)',
              overflow: 'auto',
            }}
          >
            <button
              className="absolute right-0 top-0 m-2 p-2  text-2xl text-gray-600 hover:text-gray-800"
              onClick={closeModal}
            >
              Close &#x2715;
            </button>
            <div className="flex justify-center">
              <div style={{ padding: '20px' }}>
                <Image
                  src={ParkingImage}
                  alt={'Parking Overview'}
                  className="ml-5 h-auto w-[90%]"
                ></Image>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default OpenMap;
