import { MissionService } from '@/lib/firestore/services/MissionsService';
import MissionOverview from '@/components/client/Missions/MissionOverview';
import { ParkingSpotsServiceById } from '@/lib/firestore/services/ParkingSpotsServiceById';

export default async function page() {
  const missions = await MissionService.getMissions();

  return (
    <div>
      {missions.length > 0 ? (
        <MissionOverview missions={missions} />
      ) : (
        <div className="flex h-full w-full items-center justify-center">
          <p className="text-center font-bold">No missions to display</p>
        </div>
      )}
    </div>
  );
}
