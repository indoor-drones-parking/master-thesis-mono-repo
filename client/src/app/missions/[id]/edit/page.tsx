import EditMissionForm from '@/components/client/Forms/EditMissionForm';
import { DronesService } from '@/lib/firestore/services/DronesService';
import { MissionService } from '@/lib/firestore/services/MissionsService';
import { ParkingFloorsService } from '@/lib/firestore/services/ParkingFloorService';
import React from 'react';

export default async function EditMission() {
  const drones = await DronesService.getDrones();
  const parkingFloors = await ParkingFloorsService.getParkingFloors();
  const missions = await MissionService.getMissions();

  return (
    <div>
      <h1 className="mb-8 text-center text-4xl font-bold">Edit Mission</h1>
      <EditMissionForm
        missions={missions}
          drones={drones}
        parkingFloors={parkingFloors}
      ></EditMissionForm>
    </div>
  );
}
