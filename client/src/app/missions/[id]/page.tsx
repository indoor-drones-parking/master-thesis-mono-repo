'use client';
import { database } from '@/lib/firestore/config';
import { Mission } from '@/lib/models/Mission';
import { ref, onValue, off } from 'firebase/database';
import React, { useEffect, useState } from 'react';

export default function Page({ params }: { params: { id: string } }) {
  const [mission, setMission] = useState<Mission>();

  useEffect(() => {
 
    const missionsRef = ref(database, `missions/${params.id}`);

    const handleData = (snapshot: any) => {
      const data = snapshot.val();

      if (data) {
        setMission(data);
      }
    };

    // Set up Firebase listener
    const unsubscribe = onValue(missionsRef, handleData);

    // Clean-up function
    return () => {
      // Unsubscribe from the Firebase listener when component unmounts
      off(missionsRef, 'value', handleData);
    };
  }, []); // Empty dependency array to run effect only once

  return <div>{mission?.status} </div>;
}
