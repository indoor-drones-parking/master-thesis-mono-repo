import { Drone } from '@/lib/models/Drone';
import { ParkingFloor } from '@/lib/models/ParkingFloor';
import { DronesService } from '@/lib/firestore/services/DronesService';
import { ParkingFloorsService } from '@/lib/firestore/services/ParkingFloorService';
import MissionForm from '@/components/client/Forms/MissionForm';
import Button from '@/components/server/Buttons/Button';
import { MissionService } from '@/lib/firestore/services/MissionsService';

export default async function NewMission() {
  const drones = await DronesService.getDrones();
  const parkingFloors = await ParkingFloorsService.getParkingFloors();
  const missions = await MissionService.getMissions();
  return (
    <div>
      <h1 className="mb-8 text-center text-4xl font-bold">
        Create New Mission
      </h1>
      <MissionForm
        missions={missions}
        drones={drones}
        parkingFloors={parkingFloors}
      />
    </div>
  );
}
