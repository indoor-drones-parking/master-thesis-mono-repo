'use client';
import React, { useEffect } from 'react';
import { useRouter } from 'next/navigation';
import { handleDelete, handleUpdateMark } from '@/lib/actions';
import Button from '@/components/server/Buttons/Button';

import {
  showErrorNotification,
  showSuccessNotification,
} from '@/components/client/Notifications';

interface Props {
  id: string;
}

const DeleteDeviationForm = ({ id }: Props) => {
  const router = useRouter();
  const onDelete = async () => {
    const isDeleted = await handleDelete(id);
    if (isDeleted) {
      showSuccessNotification('Deviation deleted');
      router.replace('/deviations');
    } else {
      showErrorNotification('Error while deleting');
    }
  };

  const onMarkAsFixed = async () => {
    const isMarked = await handleUpdateMark(id);
    if (isMarked) {
      showSuccessNotification('Deviation marked as fixed');
      router.replace('/deviations');
    } else {
      showErrorNotification('Error while marking deviation');
    }
  };

  return (
    <div className=" flex w-full gap-2">
      <Button
        left
        style=" text-center w-full bg-red-500 text-white hover:bg-red-700  font-bold py-2 px-4 rounded"
        onClick={onDelete}
        text="Delete"
      ></Button>
      <Button
        left
        style=" text-center w-full bg-green-500 text-white hover:bg-green-700  font-bold py-2 px-4 rounded"
        onClick={onMarkAsFixed}
        text="Mark as fixed"
      ></Button>
    </div>
  );
};

export default DeleteDeviationForm;
