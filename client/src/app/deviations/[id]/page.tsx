import React from 'react';
import DeviationCard from '@/components/server/Deviations/DevaitonCard';

import { Deviation } from '@/lib/models/Devation';
import { DeviationService } from '@/lib/firestore/services/DeviationService';
import DeleteDeviationForm from './DeleteDeviationForm';

export default async function Page({
  params,
}: {
  params: { id: string; slug: string };
}) {
  const deviation: Deviation = await DeviationService.getDeviation(params.id);

  const getConvertedTimestamp = (date: string) => {
    const dateStarted = new Date(date);
    const dateNow = new Date();
    const differenceMs = Math.abs(dateNow.getTime() - dateStarted.getTime());

    // Convert milliseconds to hours and minutes
    const hours = Math.floor(differenceMs / (1000 * 60 * 60));
    const minutes = Math.floor((differenceMs % (1000 * 60 * 60)) / (1000 * 60));

    return `${hours} h and ${minutes} min`;
  };

  return (
    <div className="">
      {deviation ? (
        <div className=" flex flex-col gap-2">
          <DeviationCard href={''} deviation={deviation} />

          <DeleteDeviationForm id={params.id} />
        </div>
      ) : (
        <p>No deviation found</p>
      )}
    </div>
  );
}
