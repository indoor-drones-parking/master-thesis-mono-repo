import React, { useEffect, useState } from 'react';

import DeviationsContainer from '@/components/server/Deviations/DeviationContainer';
import DeviationCard from '@/components/server/Deviations/DevaitonCard';
import { Deviation } from '@/lib/models/Devation';
import { DeviationService } from '@/lib/firestore/services/DeviationService';

export default async function Page() {
  const deviations: Deviation[] = await DeviationService.getDeviations();

  return (
    <div>
      <DeviationsContainer>
        {deviations ? (
          Object.values(deviations)
            .sort(
              (a, b) =>
                new Date(b.timestamp).getTime() -
                new Date(a.timestamp).getTime()
            )
            .map((deviation, index) => {
              return (
                <div
                  key={index}
                  className="group relative transform duration-300 hover:translate-x-1 hover:cursor-pointer hover:shadow-xl"
                >
                  <DeviationCard
                    href={`/deviations/${deviation.id}`}
                    deviation={deviation}
                  ></DeviationCard>

                  <div className="absolute bottom-3/4 left-1/2 mb-2 w-max -translate-x-1/2 transform rounded bg-black px-2 py-1 text-xs text-white opacity-0 transition-opacity duration-300 group-hover:opacity-100">
                    Click to see more actions
                  </div>
                </div>
              );
            })
        ) : (
          <p>No deiviatons</p>
        )}
      </DeviationsContainer>
    </div>
  );
}
