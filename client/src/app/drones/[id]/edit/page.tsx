'use client';
import { ParkingFloor } from '@/lib/models/ParkingFloor';
import { ref, onValue, off, set, push } from 'firebase/database';
import React, { useEffect, useState } from 'react';
import { database } from '@/lib/firestore/config';
import { useRouter, usePathname } from 'next/navigation';
import Button from '@/components/server/Buttons/Button';
import {
  showErrorNotification,
  showSuccessNotification,
} from '@/components/client/Notifications';
import { Drone } from '@/lib/models/Drone';

const Page = () => {
  const router = useRouter();

  const handleChange = (e: { target: { name: any; value: any } }) => {
    const { name, value } = e.target;
    const parsedValue = name === 'parkingFloorId' ? parseInt(value) : value; // Parse value to number only if it's parkingFloorId
    setFormData({ ...formData, [name]: parsedValue });
  };

  const [drone, setDrone] = useState<Drone>();
  const pathname = usePathname();
  const id = pathname.split('/')[2];
 

  useEffect(() => {
    const droneRef = ref(database, `drones/${id}`);

    const handleData = (snapshot: any) => {
      const data = snapshot.val();

      if (data) {
        setDrone(data);
        // Update formData with drone data
        setFormData({
          name: data.name,
          wifiName: data.wifiName,
          serialNumber: data.serialNumber,
        });
      }
    };

    // Set up Firebase listener
    const unsubscribe = onValue(droneRef, handleData);

    // Clean-up function
    return () => {
      // Unsubscribe from the Firebase listener when component unmounts
      off(droneRef, 'value', handleData);
    };
  }, []); // Empty dependency array to run effect only once

  const handleSubmit = (e: { preventDefault: () => void }) => {
    e.preventDefault();
    // Handle form submission here
    const nameRef = ref(database, `drones/${id}/name`);
    const serialNumRef = ref(database, `drones/${id}/serialNumber`);
    const wifinameRef = ref(database, `drones/${id}/wifiName`);

    const newDroneData = {
      name: formData.name,
      wifiName: formData.wifiName,
      serialNumber: formData.serialNumber,
    };

    // Push the updated data to the database
    try {
      set(nameRef, newDroneData.name);
      set(serialNumRef, newDroneData.serialNumber);
      set(wifinameRef, newDroneData.wifiName);
      showSuccessNotification('Drone updated');

      router.replace(`/drones/${id}`);
    } catch (err) {
      showErrorNotification('Error while updating drone');
    }
  };

  const [formData, setFormData] = useState({
    name: '',
    wifiName: '',
    serialNumber: '',
  });

  return (
    <div className="mx-auto my-8 max-w-md">
      <form
        onSubmit={handleSubmit}
        className="mb-4 rounded bg-white px-8 pb-8 pt-6 shadow-md"
      >
        <div className="mb-4">
          <label
            className="mb-2 block text-sm font-bold text-gray-700"
            htmlFor="name"
          >
            Name
          </label>
          <input
            type="text"
            name="name"
            value={formData.name}
            onChange={handleChange}
            className="focus:shadow-outline w-full appearance-none rounded border px-3 py-2 leading-tight text-gray-700 shadow focus:outline-none"
            placeholder="Enter Name"
          />
        </div>

        <div className="mb-4">
          <label
            className="mb-2 block text-sm font-bold text-gray-700"
            htmlFor="wifiName"
          >
            WiFi Name
          </label>
          <input
            type="text"
            value={formData.wifiName}
            name="wifiName"
            onChange={handleChange}
            className="focus:shadow-outline w-full appearance-none rounded border px-3 py-2 leading-tight text-gray-700 shadow focus:outline-none"
            placeholder="Enter WiFi Name"
          />
        </div>
        <div className="mb-4">
          <label
            className="mb-2 block text-sm font-bold text-gray-700"
            htmlFor="serialNumber"
          >
            Serial Number
          </label>
          <input
            type="text"
            value={formData.serialNumber}
            name="serialNumber"
            onChange={handleChange}
            className="focus:shadow-outline w-full appearance-none rounded border px-3 py-2 leading-tight text-gray-700 shadow focus:outline-none"
            placeholder="Enter Serial Number"
          />
        </div>
        <div className="flex items-center justify-between">
          <button
            onClick={handleSubmit}
            className="focus:shadow-outline rounded bg-green-500 px-4 py-2 font-bold text-white hover:bg-green-700 focus:outline-none"
            type="submit"
          >
            Update Drone
          </button>
          <Button
            style=" m-2 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
            href={'/drones/' + id}
            text="Cancel"
          ></Button>
        </div>
      </form>
    </div>
  );
};

export default Page;
