'use client';
import { useEffect, useState } from 'react';
import { ref, onValue, off, remove } from 'firebase/database';
import { database } from '@/lib/firestore/config';
import { Drone } from '@/lib/models/Drone';
import DroneCard from '@/components/server/Drones/DroneCard';
import Button from '@/components/server/Buttons/Button';
import { useRouter } from 'next/navigation';
import KeyboardArrowLeftOutlinedIcon from '@mui/icons-material/KeyboardArrowLeftOutlined';
import {
  showErrorNotification,
  showSuccessNotification,
} from '@/components/client/Notifications';

export default function Page({ params }: { params: { id: string } }) {
  const [drone, setDrone] = useState<Drone>();
  const router = useRouter();

  useEffect(() => {
    const parkingFloorsRef = ref(database, `drones/${params.id}`);

    const handleData = (snapshot: any) => {
      const data = snapshot.val();

      if (data) {
        setDrone(data);
      }
    };

    // Set up Firebase listener
    const unsubscribe = onValue(parkingFloorsRef, handleData);

    // Clean-up function
    return () => {
      // Unsubscribe from the Firebase listener when component unmounts
      off(parkingFloorsRef, 'value', handleData);
    };
  }, []); // Empty dependency array to run effect only once

  const deleteDrone = () => {
    const droneRef = ref(database, `drones/${params.id}`);
    remove(droneRef)
      .then(() => {
        showSuccessNotification('Drone deleted');

        router.push('/drones');
      })
      .catch((error) => {
        showErrorNotification('Error while deleting drone');
        console.error('Error deleting data:', error);
      });
  };

  return (
    <div className="m-2 flex w-full flex-col gap-2 ">
      <Button
        left
        style=" w-fit bg-gray-200 text-black hover:bg-gray-300  font-bold py-2 px-4 rounded"
        href="/drones"
        text="Go back"
      >
        <KeyboardArrowLeftOutlinedIcon fontSize="large" />
      </Button>
      {drone ? (
        <div className="flex w-full flex-col gap-y-5">
          <DroneCard href={''} drone={drone}></DroneCard>
          <div>
            <Button
              style="m-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
              text="Edit"
              href={'/drones/' + drone.id + '/edit'}
            ></Button>
            <Button
              onClick={deleteDrone}
              style=" m-2 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
              text="Delete"
            ></Button>
          </div>
        </div>
      ) : (
        <p> No drone found</p>
      )}
    </div>
  );
}
