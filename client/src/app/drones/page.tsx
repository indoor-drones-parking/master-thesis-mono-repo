'use client';
import { useEffect, useState } from 'react';
import { ref, onValue, off, get, child } from 'firebase/database';
import { Drone } from '@/lib/models/Drone';
import { database } from '@/lib/firestore/config';
import DroneCard from '@/components/server/Drones/DroneCard';
import FloorContainer from '@/components/server/Drones/FloorContainer';
import Button from '@/components/server/Buttons/Button';
import AddIcon from '@mui/icons-material/Add';

export default function Page() {
  const [drones, setDrones] = useState<{ [key: string]: Drone }>({});

  useEffect(() => {
    // Reference to the 'drones' node in your Firebase Realtime Database
    const dorensRef = ref(database, 'drones');

    // Function to handle data retrieval
    const handleData = (snapshot: { val: () => any }) => {
      const data = snapshot.val();

      setDrones(data); // Update the state with the retrieved data
    };

    // Set up Firebase listener
    const unsubscribe = onValue(dorensRef, handleData);

    // Clean-up function
    return () => {
      // Unsubscribe from the Firebase listener when component unmounts
      off(dorensRef, 'value', handleData);
    };
  }, []); // Empty dependency array to run effect only once

  const uniqueFloorIds = Array.from(
    new Set(Object.values(drones).map((drone) => drone.parkingFloorId))
  ).sort();

  return (
    <div>
      {/*    <Button
        style="absolute right-3 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"
        text="Register drone"
        href="/drones/register"
      >
        <AddIcon />
      </Button> */}
      <>
        {uniqueFloorIds.map((floorId, idx) => (
          <div key={floorId}>
            <FloorContainer floor={floorId} key={`${floorId}${idx}`}>
              {Object.keys(drones).map((key) => {
                const drone = drones[key];

                if (drone.parkingFloorId === floorId) {
                  return (
                    <div
                      key={drone.id}
                      className="transform overflow-hidden rounded shadow-lg duration-300 hover:translate-x-1 hover:cursor-pointer hover:shadow-xl"
                    >
                      <DroneCard drone={drone} href={`/drones/${drone.id}`} />
                    </div>
                  );
                }
                return null;
              })}
            </FloorContainer>
          </div>
        ))}
      </>
    </div>
  );
}
