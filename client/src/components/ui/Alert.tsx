import { useState, useEffect } from 'react';

interface Props {
  message: string;
}

function Alert({ message }: Props) {
  const [showNotification, setShowNotification] = useState(false);

  useEffect(() => {
    let timeoutId: string | number | NodeJS.Timeout | undefined;

    // Function to show the notification
    const showFeedbackNotification = () => {
      setShowNotification(true);
      timeoutId = setTimeout(() => {
        setShowNotification(false);
      }, 3000); // Notification disappears after 3 seconds
    };

    // Call the function to show the notification
    showFeedbackNotification();

    // Cleanup function
    return () => {
      clearTimeout(timeoutId);
    };
  }, [message]); // Re-render when message changes

  return (
    <div className={`feedback-notification ${showNotification ? 'show' : ''}`}>
      {message}
    </div>
  );
}

export default Alert;
