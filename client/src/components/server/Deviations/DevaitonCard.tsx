import Link from 'next/link';
import React from 'react';
import Image from 'next/image';
import { Deviation } from '@/lib/models/Devation';
import KeyboardArrowRightOutlinedIcon from '@mui/icons-material/KeyboardArrowRightOutlined';
import { getImageSrc } from '@/lib/minio/pictureService';
import { formatDateToString } from '@/lib/utils';

type Props = {
  href: string;
  deviation: Deviation;
};

export default async function DeviationCard({ deviation, href }: Props) {
  const imageSrc = await getImageSrc(deviation.image);
  let secondImageSrc = '';
  if (deviation.firstMappedImage != undefined) {
    secondImageSrc = await getImageSrc(deviation.firstMappedImage);
  }

  console.log(deviation.firstMappedImage && deviation.firstMappedTimestamp);

  return (
    <>
      {deviation.firstMappedImage && deviation.firstMappedTimestamp ? (
        <Link href={href}>
          <div className="relative flex overflow-hidden rounded border border-red-500 shadow-lg">
            <div className="w-full">
              <div className="px-6 py-4">
                <div className="mb-2 text-xl font-bold">
                  Issue at parking number: {deviation.parkingNumber}
                </div>

                <p className="text-base font-bold">Description: </p>
                <p className="ml-1 text-base text-gray-700">
                  {deviation.description}
                </p>
              </div>
              <div className="flex justify-between">
                <div className="flex w-1/2 flex-col content-center items-center justify-center px-6 py-4 text-center">
                  <Image
                    src={secondImageSrc} // Replace with the actual source of the second image
                    alt="Second Image Display"
                    width={400} // Adjust width and height as needed
                    height={100}
                  />
                  <p className="text-base font-bold">Timestamp: </p>
                  <p className="ml-1 text-base text-gray-700">
                    {formatDateToString(deviation.firstMappedTimestamp)}
                  </p>
                </div>
                <div className="flex w-1/2 flex-col content-center items-center justify-center px-6 py-4 text-center">
                  <Image
                    src={imageSrc}
                    alt="Image Display"
                    width={400} // Adjust width and height as needed
                    height={100}
                  />
                  <p className="text-base font-bold"> Second Timestamp: </p>
                  <p className="ml-1 text-base text-gray-700">
                    {formatDateToString(deviation.timestamp)}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </Link>
      ) : (
        <Link href={href}>
          <div className="relative flex overflow-hidden rounded border border-red-500 shadow-lg">
            <div className="w-1/2">
              <div className="px-6 py-4">
                <div className="mb-2 text-xl font-bold">
                  Issue at parking number: {deviation.parkingNumber}
                </div>

                <p className="text-base font-bold">Description: </p>
                <p className="ml-1 text-base text-gray-700">
                  {deviation.description}
                </p>
                <p className="text-base font-bold">Timestamp: </p>
                <p className="ml-1 text-base text-gray-700">
                  {formatDateToString(deviation.timestamp)}
                </p>
              </div>
            </div>
            <div className="relative flex w-1/2 justify-end px-6 py-4">
              <div className="absolute right-0 top-0 mt-2"></div>
              <Image
                src={imageSrc}
                alt="Image Display"
                width={300} // Adjust width and height as needed
                height={100}
              />
            </div>
          </div>
        </Link>
      )}
    </>
  );
}
