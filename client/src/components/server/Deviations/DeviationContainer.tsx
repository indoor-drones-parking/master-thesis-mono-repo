import React from 'react';

type Props = {
  children: React.ReactNode;
};

const DeviationsContainer = ({ children }: Props) => {
  return (
    <div className="container mx-auto px-4">
      <div className="grid grid-cols-1 gap-4  ">{children}</div>
    </div>
  );
};

export default DeviationsContainer;
