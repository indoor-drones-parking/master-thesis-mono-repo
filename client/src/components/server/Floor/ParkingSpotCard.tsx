import { ParkingSpot } from '@/lib/models/ParkingSpot';
import Link from 'next/link';
import React from 'react';

interface Props {
  parkingspot: ParkingSpot;
  floorId: string;
}

const ParkingSpotCard = ({ parkingspot, floorId }: Props) => {
  const getBorderColor = () => {
    if (parkingspot.isOccupied) {
      return 'border-2 border-red-500 flex max-w-sm flex-col items-center justify-center overflow-hidden rounded shadow-lg ';
    } else {
      return 'border-2 border-green-500 flex max-w-sm flex-col items-center justify-center overflow-hidden rounded shadow-lg';
    }
  };
  const getTimePeriod = (seconds: number) => {
    const hours = Math.floor(seconds / 3600);
    const minutes = Math.floor((seconds % 3600) / 60);

    const hoursString = hours > 0 ? `${hours} hour${hours > 1 ? 's' : ''}` : '';
    const minutesString =
      minutes > 0 ? `${minutes} minute${minutes > 1 ? 's' : ''}` : '';

    if (hoursString && minutesString) {
      return `${hoursString} and ${minutesString}`;
    } else if (hoursString) {
      return hoursString;
    } else if (minutesString) {
      return minutesString;
    } else {
      return '0 minutes';
    }
  };
  return (
    <>
      {' '}
      {parkingspot.freeTimeDurationInSeconds && parkingspot.hasFreePeriod ? (
        <Link
          href={`/parkingoverview/${floorId}/parkingspot/${parkingspot.id}`}
        >
          <div
            className={`${getBorderColor()} flex h-full flex-col justify-between`}
          >
            <div className="flex flex-grow items-center justify-center">
              <p className="p-8 text-xl font-bold">{parkingspot.number}</p>
            </div>
            <div className="mb-2 text-center text-sm">
              <p className="mb-1">
                <b>Free Time Limit: </b>
                {getTimePeriod(parkingspot.freeTimeDurationInSeconds)}
              </p>
              <p>{parkingspot.hasFreePeriod}</p>
            </div>
          </div>
        </Link>
      ) : (
        <Link
          href={`/parkingoverview/${floorId}/parkingspot/${parkingspot.id}`}
        >
          <div
            className={`${getBorderColor()} flex h-full flex-col justify-center`}
          >
            <div className="flex flex-grow items-center justify-center">
              <p className="p-8 text-xl font-bold">{parkingspot.number}</p>
            </div>
          </div>
        </Link>
      )}
    </>
  );
};

export default ParkingSpotCard;
