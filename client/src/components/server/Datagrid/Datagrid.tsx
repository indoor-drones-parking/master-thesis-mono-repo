'use client';
import React from 'react';
import Box from '@mui/material/Box';
import { DataGrid as MuiDataGrid, GridColDef } from '@mui/x-data-grid';
import { useToast } from '@/components/ui/use-toast';

type DataGridProps<T> = {
  data: T[];
  columns: GridColDef[];
  onRowUpdate?: (row: T) => Promise<boolean>;
};

export default function DataGrid<T>({
  data,
  columns,
  onRowUpdate,
}: DataGridProps<T>) {
  const { toast } = useToast();
  return (
    <Box sx={{ height: 'calc(100vh - 8rem)', width: '40%' }}>
      <MuiDataGrid
        rows={data}
        columns={columns}
        pageSizeOptions={[5, 10, 25, 50, 100]}
        disableRowSelectionOnClick
        processRowUpdate={async (updatedRow, originalRow) => {
          if (onRowUpdate) {
            const isUpdated = await onRowUpdate(updatedRow);
            if (isUpdated) return updatedRow;
          }
        }}
        onProcessRowUpdateError={(error) => {
          toast({
            variant: 'destructive',
            title: 'The update failed',
          });
        }}
      />
    </Box>
  );
}
