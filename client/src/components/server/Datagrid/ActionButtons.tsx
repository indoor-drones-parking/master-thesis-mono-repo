import { IconButton } from '@mui/material';
import React from 'react';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import Link from 'next/link';

type Props = {
  handleDelete: () => void;
  editRoute: string;
};

function ActionButtons({ editRoute, handleDelete }: Props) {
  return (
    <React.Fragment>
      <Link href={editRoute}>
        <IconButton color="inherit">
          <EditIcon />
        </IconButton>
      </Link>
      <IconButton onClick={handleDelete} color="inherit">
        <DeleteIcon />
      </IconButton>
    </React.Fragment>
  );
}

export default ActionButtons;
