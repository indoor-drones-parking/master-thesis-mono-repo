'use client';
import React, { useState } from 'react';
import KeyboardArrowDownOutlinedIcon from '@mui/icons-material/KeyboardArrowDownOutlined';
import KeyboardArrowUpOutlinedIcon from '@mui/icons-material/KeyboardArrowUpOutlined';

type Props = {
  label: string;
  icon: React.ReactNode;
  children: React.ReactNode;
};

export function SidebarItemWithChildren({ children, icon, label }: Props) {
  const [isOpen, setOpen] = useState<boolean>(false);
  return (
    <React.Fragment>
      <ul
        onClick={() => setOpen(!isOpen)}
        className={`relative mb-2 flex h-12 items-center justify-start gap-x-1 rounded-lg px-4 text-white transition-all duration-200 ease-in-out hover:bg-primary-80`}
      >
        {icon}
        {label}
        <div className="ml-auto">
          {isOpen ? (
            <KeyboardArrowDownOutlinedIcon />
          ) : (
            <KeyboardArrowUpOutlinedIcon />
          )}
        </div>
      </ul>
      {isOpen && children}
    </React.Fragment>
  );
}
