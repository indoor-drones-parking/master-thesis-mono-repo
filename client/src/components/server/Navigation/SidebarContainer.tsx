import React from 'react';

type Props = {
  isOpen: boolean;
  children: React.ReactNode;
};

export const SidebarContainer = ({ isOpen, children }: Props) => {
  return (
    <aside
      className={`bg-primary fixed bottom-0 left-0 z-50 flex h-sidebar w-sidebar transform flex-col
                  overflow-auto transition-transform duration-300 ${
                    isOpen ? 'translate-x-0' : '-translate-x-full'
                  }`}
    >
      {children}
    </aside>
  );
};
