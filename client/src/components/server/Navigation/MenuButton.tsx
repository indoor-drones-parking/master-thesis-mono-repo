import MenuOpenIcon from '@mui/icons-material/MenuOpen';
import MenuIcon from '@mui/icons-material/Menu';

type Props = {
  open: boolean;
};

export const MenuButton = ({ open }: Props) => {
  if (open) {
    return <MenuOpenIcon />;
  }
  return <MenuIcon />;
};
