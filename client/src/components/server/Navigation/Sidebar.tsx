import { SidebarContainer } from './SidebarContainer';
import TaskOutlinedIcon from '@mui/icons-material/TaskOutlined';
import SidebarItem from '../../client/Navigation/SidebarItem';
import CameraswitchOutlinedIcon from '@mui/icons-material/CameraswitchOutlined';
import { SidebarItemWithChildren } from './SidebarItemWithChildren';
import { ParkingFloorDto } from '@/lib/models/ParkingFloor';
import PendingActionsIcon from '@mui/icons-material/PendingActions';
import ReportProblemIcon from '@mui/icons-material/ReportProblem';
import NotificaitonCircle from '@/components/client/Navigation/NotificaitonCircle';

type SidebarProps = {
  isSidebarOpen: boolean;
};

const getParkingFloors = (): ParkingFloorDto[] => {
  return [
    {
      id: 'parkingFloorId1',
      number: 1,
    },
    {
      id: 'parkingFloorId2',
      number: 2,
    },
  ];
};

export const Sidebar = ({ isSidebarOpen }: SidebarProps) => {
  const parkingFloors: ParkingFloorDto[] = getParkingFloors();

  return (
    <SidebarContainer isOpen={isSidebarOpen}>
      <nav className="relative flex min-w-full flex-col">
        <ul className="m-0 mt-1 list-none p-0 px-2">
          <SidebarItemWithChildren
            label="Parking Overview"
            icon={<TaskOutlinedIcon />}
          >
            {parkingFloors.map((floor) => (
              <SidebarItem
                key={floor.id}
                isChild
                href={`/parkingoverview/${floor.id}`}
              >
                Floor {floor.number}
              </SidebarItem>
            ))}
          </SidebarItemWithChildren>
          <SidebarItem href={`/drones`}>
            <CameraswitchOutlinedIcon />
            Drones
          </SidebarItem>

          <SidebarItemWithChildren
            label="Missions"
            icon={<PendingActionsIcon />}
          >
            <SidebarItem isChild href={`/missions`}>
              Missions
            </SidebarItem>
            <SidebarItem isChild href={`/missions/newMission`}>
              New mission
            </SidebarItem>
          </SidebarItemWithChildren>

          <SidebarItem href={`/deviations`}>
            <ReportProblemIcon />
            Deviations
            <NotificaitonCircle />
          </SidebarItem>
        </ul>
      </nav>
    </SidebarContainer>
  );
};
