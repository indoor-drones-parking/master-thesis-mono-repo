import logo from '@/assets/logos/amado-logo.svg';
import Image from 'next/image';
import { MenuButton } from './MenuButton';

type HeaderProps = {
  onToggleSidebar: () => void;
  isSidebarOpen: boolean;
};

export const Header = ({ onToggleSidebar, isSidebarOpen }: HeaderProps) => {
  return (
    <>
      <div className="fixed left-0 top-0 z-50 flex  h-header w-full bg-white">
        <div className="transition-width duration-225 flex w-full  items-center shadow ease-in">
          <button
            className=" relative flex h-full cursor-pointer items-center border-0 bg-transparent px-5"
            onClick={onToggleSidebar}
          >
            <MenuButton open={isSidebarOpen} />
          </button>
          <div className="duration-225 bg-primary-20 flex w-sidebar translate-x-0 transform items-center justify-center border-b border-white transition-transform ease-in-out">
            <Image src={logo} alt="InSite logo" width={120}></Image>
          </div>
        </div>
      </div>
    </>
  );
};
