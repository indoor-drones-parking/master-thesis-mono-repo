import { ReactNode } from 'react';

type Props = {
  isSidebarOpen: boolean;
  children: ReactNode;
};

export function PageContainer({ isSidebarOpen, children }: Props) {
  return (
    <main
      className={`
        min-h-page)] transition-margin transition-width bg-Background
        relative mx-auto mt-header box-border min-h-page
        p-1 duration-300 ease-in-out
        md:p-1 lg:p-2 xl:p-4 2xl:p-8
        ${isSidebarOpen ? `ml-sidebar w-page` : 'ml-0 w-full'}
      `}
    >
      {children}
    </main>
  );
}
