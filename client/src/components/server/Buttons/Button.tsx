import Link from 'next/link';
import React, { ReactNode } from 'react';

type Props = {
  href?: string;
  style: string;
  text?: string;
  children?: React.ReactNode;
  onClick?: () => void;
  left?: boolean;
};

const Button = ({ href, style, text, children, onClick, left }: Props) => {
  const pressed = () => {
    if (onClick != undefined) onClick();
  };
  return (
    <>
      {left ? (
        <Link className={style} onClick={pressed} href={href ?? '#'}>
          {children}
          {text ?? 'button'}
        </Link>
      ) : (
        <Link className={style} onClick={pressed} href={href ?? '#'}>
          {text ?? 'button'} {children}
        </Link>
      )}
    </>
  );
};

export default Button;
