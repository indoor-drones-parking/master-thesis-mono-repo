import Link from 'next/link';
import React from 'react';
import { Drone } from '@/lib/models/Drone';
import TelloImage from '../../../assets/images/tello.jpg';
import Image from 'next/image';

type Props = {
  href: string;
  drone: Drone;
};
const DroneCard = ({ drone, href }: Props) => {
  const getColor = (value: number): string => {
    if (value >= 50 && value <= 100) {
      return '"m-1 flex cursor-default items-center justify-center  py-4 text-center text-xs font-bold leading-none text-white" bg-green-400';
    } else if (value >= 30 && value <= 50) {
      return '"m-1 flex cursor-default items-center justify-center  py-4 text-center text-xs font-bold leading-none text-white" bg-yellow-400';
    } else {
      return '"m-1 flex cursor-default items-center justify-center  py-4 text-center text-xs font-bold leading-none text-white" bg-red-400';
    }
  };
  return (
    <Link href={href}>
      <div className="max-w-sm overflow-hidden rounded shadow-lg ">
        <Image
          src={TelloImage}
          width={500}
          height={500}
          alt="Picture of the author"
        />
        <div className="px-6 py-4">
          <div className="mb-2 text-xl font-bold">{drone.name}</div>
          <p className="text-[10px] font-bold">Misson: </p>
          <p className="ml-1 text-base text-gray-700">{drone.operation}</p>
          <div className="group relative transform duration-300  hover:cursor-default">
            <p className="text-[10px] font-bold">Status: </p>
            <p className="ml-1 text-base text-gray-700"> {drone.status}</p>
            {drone.status === 'IDLE' ? (
              <div className="absolute bottom-1/4 left-2/3 mb-2 w-max -translate-x-1/2 transform rounded bg-black px-2 py-1 text-xs text-white opacity-0 transition-opacity duration-300 group-hover:opacity-100">
                Drone not in use.
              </div>
            ) : (
              <div className="absolute bottom-3/4 left-1/2 mb-2 w-max -translate-x-1/2 transform rounded bg-black px-2 py-1 text-xs text-white opacity-0 transition-opacity duration-300 group-hover:opacity-100">
                Drone in use.
              </div>
            )}
          </div>
          <p className="text-[10px] font-bold">Battery: </p>
          <div className="relative w-full">
            <div className={getColor(drone.batteryPercentage)}>
              <div className="absolute  text-gray-700">
                {drone.batteryPercentage} %
              </div>
            </div>
          </div>
        </div>
      </div>
    </Link>
  );
};

export default DroneCard;
