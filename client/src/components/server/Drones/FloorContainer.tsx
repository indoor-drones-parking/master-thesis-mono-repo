import React from 'react';

type Props = {
  children: React.ReactNode;
  floor: string;
};

const FloorContainer = ({ children, floor }: Props) => {
  return (
    <div>
      <h1 className="m-2 text-xl font-bold">
        Floor {floor.charAt(floor.length - 1)}
      </h1>
      <div className="container mx-auto px-4">
        <div className="grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
          {children}
        </div>
      </div>
    </div>
  );
};

export default FloorContainer;
