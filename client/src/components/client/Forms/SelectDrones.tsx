import { Drone } from '@/lib/models/Drone';
import React, { useState } from 'react';
import CloseIcon from '@mui/icons-material/Close';

interface Props {
  drones: Drone[];
  selectedDrones: Drone[];
  setSelectedDrones: React.Dispatch<React.SetStateAction<Drone[]>>;
}

const SelectDrones = ({ drones, selectedDrones, setSelectedDrones }: Props) => {
  const [selectedDroneId, setSelectedDroneId] = useState<string | null>(null);

  const handleAddSelectedDrone = (id: string) => {
    const selectedDrone = drones.find((drone) => drone.id === id);
    if (selectedDrone && !selectedDrones.includes(selectedDrone)) {
      // Temporarily set operation to 'NONE'
      selectedDrone.operation = 'NONE';
      setSelectedDrones([...selectedDrones, selectedDrone]);
    }
    setSelectedDroneId(null); // Reset selected drone after adding
  };

  const handleRemoveSelectedDrone = (id: string) => {
    const removedDroneIndex = selectedDrones.findIndex(
      (drone) => drone.id === id
    );
    if (removedDroneIndex !== -1) {
      // Revert operation back to its original value
      const updatedSelectedDrones = [...selectedDrones];
      updatedSelectedDrones[removedDroneIndex].operation = 'NONE';
      setSelectedDrones(
        updatedSelectedDrones.filter((drone) => drone.id !== id)
      );
    }
  };

  const renderSelectedDrones = () => {
    return (
      <div className="mt-2 flex flex-wrap">
        {selectedDrones
          .sort((a, b) => a.name.localeCompare(b.name))
          .map((drone) => (
            <div
              key={drone.id + 'as'}
              className="m-1 flex items-center rounded-full bg-gray-200 px-3 py-1"
            >
              <span>{drone.name}</span>

              <button
                onClick={() => handleRemoveSelectedDrone(drone.id)}
                className="ml-2"
              >
                <CloseIcon />
              </button>
            </div>
          ))}
      </div>
    );
  };

  return (
    <div className="mb-4">
      <label className="mb-2 block text-sm font-bold text-gray-700">
        Select Drones:
      </label>
      <div className="relative">
        <select
          className="focus:shadow-outline w-full appearance-none rounded border px-3 py-2 shadow focus:outline-none"
          value={selectedDroneId || ''}
          onChange={(e) => handleAddSelectedDrone(e.target.value)}
        >
          <option value="" disabled hidden>
            Select drones
          </option>
          {drones
            .filter(
              (drone) =>
                !selectedDrones.includes(drone) && drone.operation === 'NONE'
            )
            .sort((a, b) => a.name.localeCompare(b.name)) // Sort drones alphabetically
            .map((drone, idx) => (
              <option key={drone.id + idx} value={drone.id}>
                {drone.name}
              </option>
            ))}
          {drones.filter(
            (drone) =>
              !selectedDrones.includes(drone) && drone.operation === 'NONE'
          ).length < 1 && <option>No available drones</option>}
        </select>
      </div>
      {renderSelectedDrones()}
      {selectedDrones.length > 0 ? (
        <p>Total drones selected: {selectedDrones.length}</p>
      ) : (
        <></>
      )}
    </div>
  );
};

export default SelectDrones;
