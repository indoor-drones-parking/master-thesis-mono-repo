import React from 'react';

interface Props {
  setMapperInterval: React.Dispatch<React.SetStateAction<number>>;
  mapperInterval: number;
}

const SelectInterval = ({ setMapperInterval, mapperInterval }: Props) => {
  const handleIntervalChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = parseInt(event.target.value);
    setMapperInterval(isNaN(value) ? 0 : value);
  };

  return (
    <div className="mb-4">
      <label
        className="mb-2 block text-sm font-bold text-gray-700"
        htmlFor="intervalInput"
      >
        Select mapping interval (minutes)
      </label>
      <input
        type="number" 
        id="intervalInput"
        value={mapperInterval} 
        onChange={handleIntervalChange} 
        className="focus:shadow-outline w-full appearance-none rounded border px-3 py-2 leading-tight text-gray-700 shadow focus:outline-none"
        placeholder="Enter interval"
      />
    </div>
  );
};

export default SelectInterval;
