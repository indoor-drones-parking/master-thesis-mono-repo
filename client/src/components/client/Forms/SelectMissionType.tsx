import React from 'react';

interface Props {
  missionType: string;
  setMissionType: React.Dispatch<React.SetStateAction<string>>;
}

const SelectMissionType = ({ missionType, setMissionType }: Props) => {
  return (
    <div className="mb-4">
      <label
        className="mb-2 block text-sm font-bold text-gray-700"
        htmlFor="missionType"
      >
        Mission Type:
      </label>
      <select
        id="missionType"
        value={missionType}
        onChange={(e) => setMissionType(e.target.value)}
        className="focus:shadow-outline w-full cursor-pointer appearance-none rounded border px-3 py-2 shadow focus:outline-none"
      >
        <option value="">Select Mission Type</option>
        <option value="GUIDER">Guider</option>
        <option value="MAPPER">Mapper</option>
      </select>
    </div>
  );
};

export default SelectMissionType;
