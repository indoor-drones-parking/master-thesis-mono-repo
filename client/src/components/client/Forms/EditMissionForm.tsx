'use client';
import React, { useEffect, useState } from 'react';
import { Drone, DroneOperation } from '@/lib/models/Drone';
import SelectFloor from './SelectFloor';
import SelectDrones from './SelectDrones';
import SelectMissionType from './SelectMissionType';
import SelectParkingSpotRange from './SelectParkingSpotRange';
import { ParkingFloors } from '@/lib/models/ParkigFloors';
import SelectInterval from './SelectInterval';
import Button from '@/components/server/Buttons/Button';
import { database } from '@/lib/firestore/config';
import { get, push, ref, set, update } from 'firebase/database';
import { GuiderMission, MapperMission, Mission } from '@/lib/models/Mission';
import { usePathname, useRouter } from 'next/navigation';
import {
  showErrorNotification,
  showSuccessNotification,
} from '../Notifications';
import { MissionService } from '@/lib/firestore/services/MissionsService';

interface Props {
  drones: Drone[];
  parkingFloors: ParkingFloors;
  missions: Mission[];
}

const EditMissionForm = ({ drones, parkingFloors, missions }: Props) => {
  const [loading, setLoading] = useState(true);
  const pathname = usePathname();
  const id = pathname.split('/')[2];
  const [missionType, setMissionType] = useState(''); // Initialize missionType with mission's type
  const [selectedDrones, setSelectedDrones] = useState<Drone[]>([]); // Initialize selectedDrones with mission's droneIds
  const [selectedFloor, setSelectedFloor] = useState(''); // Initialize selectedFloor with mission's parkingFloorId
  const [selectedSpots, setSelectedSpots] = useState<string[]>([]); // Initialize selectedSpots with mission's parkingSpotsIds
  const [mapperInterval, setMapperInterval] = useState(0); // Initialize mapperInterval with mission's mapperInterval
  const [initialSelectedDrones, setInitialSelectedDrones] = useState<Drone[]>(
    []
  );
  const router = useRouter();

  useEffect(() => {
    // Fetch mission data when component mounts
    async function fetchMissionData() {
      try {
        const missionSnapshot = await get(ref(database, `missions/${id}`));
        if (missionSnapshot.exists()) {
          const missionData = missionSnapshot.val();
          console.log(missionData);
          // Update state with mission data
          setMissionType(missionData.type);
          const selectedDronesData = missionData.droneIds.map(
            (droneId: string) =>
              drones.find((drone: Drone) => drone.id === droneId)
          );
          setSelectedDrones(selectedDronesData.filter(Boolean));
          setInitialSelectedDrones(selectedDronesData.filter(Boolean));
          setSelectedFloor(missionData.parkingFloorId || '');

          setSelectedSpots(missionData.parkingSpotsIds);
          setMapperInterval(missionData.mapperInterval);
          setLoading(false);
        }
      } catch (error) {
        console.error('Error fetching mission data:', error);
      }
    }
    fetchMissionData();
  }, [id]); // Fetch data whenever mission id changes

  // Function to check if all required fields are selected
  const isFormValid = () => {
    if (selectedDrones.length === 0) return false;
    if (
      missionType === 'MAPPER' &&
      (mapperInterval === 0 || selectedSpots.length === 0)
    )
      return false;
    return true;
  };

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const date = new Date(Date.now());
    const missionsRef = ref(database, `missions/${id}`);

    let newMissionData: any = {};

    if (missionType === 'MAPPER') {
      newMissionData = {
        type: missionType as DroneOperation,
        droneIds: selectedDrones.map((drone) => drone.id),
        parkingFloorId: selectedFloor,
        parkingSpotsIds: selectedSpots,
        mapperInterval: mapperInterval,
        timeStarted: date.toString(),
        nextParkingSpot: selectedSpots[0],
        id: id,
      };
    } else if (missionType === 'GUIDER') {
      newMissionData = {
        type: missionType as DroneOperation,
        droneIds: selectedDrones.map((drone) => drone.id),
        parkingFloorId: selectedFloor,
        timeStarted: date.toString(),
        id: id,
      };
    }

    try {
      update(missionsRef, newMissionData);
      initialSelectedDrones.forEach(async (drone) => {
        // Check if the drone is present in selectedDrones
        const found = selectedDrones.some(
          (selectedDrone) => selectedDrone.id === drone.id
        );

        // If the drone is not present in selectedDrones, set its operation to "NONE"
        if (!found) {
          const droneRef = ref(database, `drones/${drone.id}/operation`);
          const droneMissionIdRef = ref(
            database,
            `drones/${drone.id}/missionId`
          );
          const droneParkingFloorIdRef = ref(
            database,
            `drones/${drone.id}/parkingFloorId`
          );
          await set(droneRef, 'NONE');
          await set(droneMissionIdRef, '');
          await set(droneParkingFloorIdRef, newMissionData.parkingFloorId);
        }
      });

      selectedDrones.forEach(async (drone) => {
        const droneRef = ref(database, `drones/${drone.id}/operation`);
        const droneMissionIdRef = ref(database, `drones/${drone.id}/missionId`);
        await set(droneRef, missionType);
        await set(droneMissionIdRef, id);
      });
      showSuccessNotification('Mission Updated');
      router.push('/missions');
      router.refresh();
    } catch (error) {
      showErrorNotification('Error While Updating Mission');
      console.error('Error adding new mission:', error);
    }
  };

  return (
    <div className="relative mx-auto my-8 max-w-md">
      <form
        className="mb-4 rounded bg-white px-8 pb-8 pt-6 shadow-md"
        onSubmit={handleSubmit}
      >
        <SelectMissionType
          missionType={missionType}
          setMissionType={setMissionType}
        />

        {missionType === 'MAPPER' && (
          <>
            <SelectDrones
              selectedDrones={selectedDrones}
              drones={drones}
              setSelectedDrones={setSelectedDrones}
            />
            <SelectFloor
              setSelectedFloor={setSelectedFloor}
              selectedFloor={selectedFloor}
              parkingFloors={parkingFloors}
            />

            <SelectParkingSpotRange
              selectedFloor={parkingFloors[selectedFloor]}
              selectedSpots={selectedSpots}
              setSelectedSpots={setSelectedSpots}
              missions={missions}
            />
            {/* 
            <SelectInterval
              mapperInterval={mapperInterval}
              setMapperInterval={setMapperInterval}
            /> */}
          </>
        )}

        {missionType === 'GUIDER' && (
          <>
            <SelectDrones
              selectedDrones={selectedDrones}
              drones={drones}
              setSelectedDrones={setSelectedDrones}
            />
            <SelectFloor
              setSelectedFloor={setSelectedFloor}
              selectedFloor={selectedFloor}
              parkingFloors={parkingFloors}
            />
          </>
        )}

        <button
          type="submit"
          disabled={!isFormValid()} // Disable the button if the form is not valid
          className={`focus:shadow-outline w-full rounded bg-green-500 px-4 py-2 font-bold text-white hover:bg-green-700 focus:outline-none ${!isFormValid() ? 'cursor-not-allowed opacity-50' : ''}`}
        >
          Update Mission
        </button>
        <Button
          href="/missions"
          text="Cancel"
          style="absolute right-0 top-0 m-2 focus:shadow-outline rounded bg-red-500 px-4 py-2 font-bold text-white hover:bg-red-700 focus:outline-none"
        ></Button>
      </form>
    </div>
  );
};

export default EditMissionForm;
