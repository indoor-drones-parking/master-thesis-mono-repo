'use client';
import React, { useEffect, useState } from 'react';
import { Drone, DroneOperation } from '@/lib/models/Drone';
import SelectFloor from './SelectFloor';
import SelectDrones from './SelectDrones';
import SelectMissionType from './SelectMissionType';
import SelectParkingSpotRange from './SelectParkingSpotRange';
import { ParkingFloors } from '@/lib/models/ParkigFloors';
import SelectInterval from './SelectInterval';
import Button from '@/components/server/Buttons/Button';
import { database } from '@/lib/firestore/config';
import { push, ref, set } from 'firebase/database';
import { GuiderMission, MapperMission, Mission } from '@/lib/models/Mission';
import { useRouter } from 'next/navigation';
import {
  showErrorNotification,
  showSuccessNotification,
} from '../Notifications';

interface Props {
  drones: Drone[];
  parkingFloors: ParkingFloors;
  missions: Mission[];
}

const MissionForm = ({ drones, parkingFloors, missions }: Props) => {
  const [missionType, setMissionType] = useState('');
  const [selectedDrones, setSelectedDrones] = useState<Drone[]>([]);
  const [selectedFloor, setSelectedFloor] = useState(
    Object.keys(parkingFloors)[0]
  );
  const [selectedSpots, setSelectedSpots] = useState<string[]>([]);
  const [mapperInterval, setMapperInterval] = useState(5);
  const router = useRouter();

  // Function to check if all required fields are selected
  const isFormValid = () => {
    if (selectedDrones.length === 0) return false;
    if (
      missionType === 'MAPPER' &&
      (mapperInterval === 0 || selectedSpots.length === 0)
    )
      return false;
    return true;
  };

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const date = new Date(Date.now());
    const missionsRef = ref(database, 'missions');

    let newMissionData: any = {};

    if (missionType === 'MAPPER') {
      newMissionData = {
        status: 'IDLE',
        type: missionType as DroneOperation,
        droneIds: selectedDrones.map((drone) => drone.id),
        parkingFloorId: selectedFloor,
        parkingSpotsIds: selectedSpots,
        mapperInterval: mapperInterval,
        timeStarted: date.toString(),
        nextParkingSpot: selectedSpots[1],
        id: 'temp',
      };
    } else if (missionType === 'GUIDER') {
      newMissionData = {
        status: 'IDLE',
        type: missionType as DroneOperation,
        droneIds: selectedDrones.map((drone) => drone.id),
        parkingFloorId: selectedFloor,
        timeStarted: date.toString(),
        id: 'temp',
      };
    }

    try {
      const newMissionRef = await push(missionsRef, newMissionData);
      const newMissionId = newMissionRef.key;

      if (newMissionId) {
        newMissionData = { ...newMissionData, id: newMissionId };
        set(newMissionRef, newMissionData);

        selectedDrones.forEach(async (drone) => {
          const operationRef = ref(database, `drones/${drone.id}/operation`);
          const droneMissionIdRef = ref(
            database,
            `drones/${drone.id}/missionId`
          );
          const droneParkingFloorIdRef = ref(
            database,
            `drones/${drone.id}/parkingFloorId`
          );
          await set(operationRef, missionType);
          await set(droneMissionIdRef, newMissionId);
          await set(droneParkingFloorIdRef, newMissionData.parkingFloorId);
        });
        showSuccessNotification('Mission Created');
        router.push('/missions');
        router.refresh();
      } else {
        console.error('Failed to retrieve ID for the new mission.');
      }
    } catch (error) {
      showErrorNotification('Error while creation mission');
      console.error('Error adding new mission:', error);
    }
  };

  return (
    <div className="relative mx-auto my-8 max-w-md">
      <form
        className="mb-4 rounded bg-white px-8 pb-8 pt-6 shadow-md"
        onSubmit={handleSubmit}
      >
        <SelectMissionType
          missionType={missionType}
          setMissionType={setMissionType}
        />

        {missionType === 'MAPPER' && (
          <>
            <SelectDrones
              selectedDrones={selectedDrones}
              drones={drones}
              setSelectedDrones={setSelectedDrones}
            />
            <SelectFloor
              setSelectedFloor={setSelectedFloor}
              selectedFloor={selectedFloor}
              parkingFloors={parkingFloors}
            />
            <SelectParkingSpotRange
              selectedFloor={parkingFloors[selectedFloor]}
              selectedSpots={selectedSpots}
              setSelectedSpots={setSelectedSpots}
              missions={missions}
            />
            {/*        <SelectInterval
              mapperInterval={mapperInterval}
              setMapperInterval={setMapperInterval}
            /> */}
          </>
        )}

        {missionType === 'GUIDER' && (
          <>
            <SelectDrones
              selectedDrones={selectedDrones}
              drones={drones}
              setSelectedDrones={setSelectedDrones}
            />
            <SelectFloor
              setSelectedFloor={setSelectedFloor}
              selectedFloor={selectedFloor}
              parkingFloors={parkingFloors}
            />
          </>
        )}

        <button
          type="submit"
          disabled={!isFormValid()} // Disable the button if the form is not valid
          className={`focus:shadow-outline w-full rounded bg-green-500 px-4 py-2 font-bold text-white hover:bg-green-700 focus:outline-none ${!isFormValid() ? 'cursor-not-allowed opacity-50' : ''}`}
        >
          Create Mission
        </button>
        <Button
          href="/missions"
          text="Cancel"
          style="absolute right-0 top-0 m-2 focus:shadow-outline rounded bg-red-500 px-4 py-2 font-bold text-white hover:bg-red-700 focus:outline-none"
        ></Button>
      </form>
    </div>
  );
};

export default MissionForm;
