import { ParkingFloor } from '@/lib/models/ParkingFloor';
import { ParkingSpot } from '@/lib/models/ParkingSpot';
import { MapperMission, Mission } from '@/lib/models/Mission';
import React, { useEffect, useState } from 'react';

interface Props {
  selectedFloor: ParkingFloor;
  selectedSpots: string[];
  setSelectedSpots: React.Dispatch<React.SetStateAction<string[]>>;
  missions: Mission[];
}

const SelectParkingSpotRange = ({
  selectedFloor,
  setSelectedSpots,
  selectedSpots,
  missions,
}: Props) => {
  const [spotsInMission, setSpotsInMission] = useState<string[]>([]);

  useEffect(() => {
    setSpotsInMission([...selectedSpots]);
  }, []);

  useEffect(() => {
    setSelectedSpots([]);
  }, [selectedFloor]);

  // Sort parking spots based on their number
  const parkingSpots: ParkingSpot[] = selectedFloor
    ? Object.values(selectedFloor).sort((a, b) => a.number - b.number)
    : [];

  const handleSelectSpot = (spotId: string) => {
    // Check if the spot is already selected
    if (selectedSpots.includes(spotId)) {
      // Deselect spot
      setSelectedSpots(selectedSpots.filter((id) => id !== spotId));
    } else {
      // Add spot to selected list
      setSelectedSpots(
        [...selectedSpots, spotId].sort((a, b) => parseInt(a) - parseInt(b))
      );
    }
  };

  const renderParkingSpots = () => {
    // Check if missions is defined
    if (!missions) return null;

    // Extract all used parking spot IDs from missions
    const usedParkingSpotIds = missions
      .flatMap((mission) => (mission as MapperMission).parkingSpotsIds)
      .filter((id) => !spotsInMission.includes(id));

    // Filter out parking spots that are already used in missions
    const availableParkingSpots = parkingSpots.filter((spot) => {
      return !usedParkingSpotIds.includes(spot.id);
    });
    if (availableParkingSpots.length > 0) {
      return (
        <div className="mt-2 flex flex-wrap">
          {availableParkingSpots.map((spot: ParkingSpot) => (
            <button
              type="button"
              key={spot.id}
              onClick={() => handleSelectSpot(spot.id)}
              className={`m-1 flex items-center rounded-full px-3 py-1 ${
                selectedSpots.includes(spot.id)
                  ? 'bg-blue-500 text-white'
                  : 'bg-gray-200 text-gray-700'
              }`}
            >
              <span>{spot.number}</span>
            </button>
          ))}
        </div>
      );
    } else {
      return <p> No available parking spots at this floor</p>;
    }
  };

  return (
    <div className="mb-4">
      <label className="mb-2 block text-sm font-bold text-gray-700">
        Select Parking Spots:
      </label>
      {renderParkingSpots()}
      {selectedSpots.length > 0 && (
        <div className="mt-2">
          <p>
            Selected spots:{' '}
            {selectedSpots
              .map((id) => selectedFloor[id]?.number) // Use optional chaining to handle undefined values
              .filter((number) => typeof number === 'number') // Filter out undefined values
              .sort((a, b) => (a || 0) - (b || 0)) // Sort the numbers
              .join(', ')}
          </p>
        </div>
      )}
    </div>
  );
};

export default SelectParkingSpotRange;
