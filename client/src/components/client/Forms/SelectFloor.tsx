import { ParkingFloors } from '@/lib/models/ParkigFloors';

import React from 'react';

interface Props {
  selectedFloor: string;
  setSelectedFloor: React.Dispatch<React.SetStateAction<string>>;
  parkingFloors: ParkingFloors;
}
const SelectFloor = ({
  selectedFloor,
  setSelectedFloor,
  parkingFloors,
}: Props) => {
  return (
    <div className="mb-4">
      <label
        className="mb-2 block text-sm font-bold text-gray-700"
        htmlFor="selectedFloor"
      >
        Select Parking Floor:
      </label>
      <select
        id="selectedFloor"
        value={selectedFloor}
        onChange={(e) => setSelectedFloor(e.target.value)}
        className="focus:shadow-outline w-full appearance-none rounded border px-3 py-2 shadow focus:outline-none"
      >
        {Object.keys(parkingFloors).map((parkingFloorId) => {
          return (
            <option key={parkingFloorId} value={parkingFloorId}>
              {parseFloat(parkingFloorId.charAt(parkingFloorId.length - 1))}
            </option>
          );
        })}
      </select>
    </div>
  );
};

export default SelectFloor;
