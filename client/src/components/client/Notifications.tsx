'use client';
import { CheckCircle, Warning } from '@mui/icons-material';
import { Box } from '@mui/material';
import Icon from '@mui/material/Icon';
import toast, { Toaster } from 'react-hot-toast';

type ToastContentProps = {
  message: string;
  onClose?: () => void;
};

export function ToastContent({ message, onClose }: ToastContentProps) {
  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="space-between"
      gap={3}
    >
      {message}
    </Box>
  );
}

export function showInfoNotification(message: string) {
  toast((t) => (
    <ToastContent message={message} onClose={() => closeToast(t.id)} />
  ));
}

export function showSuccessNotification(message: string) {
  toast.success((t) => (
    <ToastContent message={message} onClose={() => closeToast(t.id)} />
  ));
}

export function showErrorNotification(message: string) {
  toast.error((t) => {
    return <ToastContent message={message} onClose={() => closeToast(t.id)} />;
  });
}

export function closeToast(toastId: string) {
  toast.dismiss(toastId);
}

export function Notifications() {
  return (
    <Toaster
      position="bottom-right"
      toastOptions={{
        duration: 10000,
        style: {
          padding: 12,
          borderRadius: 12,
          color: Colors.Primary,
          maxWidth: '70ch',
          background: Colors.Info,
        },
        success: {
          icon: (
            <Icon
              component={CheckCircle}
              style={{
                color: Colors.SuccessDark,
                alignSelf: 'start',
                marginTop: 6,
              }}
            />
          ),
          style: {
            background: Colors.Success,
          },
          iconTheme: {
            primary: Colors.SuccessDark,
            secondary: Colors.White,
          },
        },
        error: {
          icon: (
            <Icon
              component={Warning}
              style={{
                color: Colors.DangerDark,
                alignSelf: 'start',
                marginTop: 6,
              }}
            />
          ),
          style: {
            background: Colors.Danger,
            alignSelf: 'start',
          },
        },
      }}
    />
  );
}

export const Colors = {
  Background: '#fdfdfd',
  HeaderBackground: '#fdfdfd',
  SidebarHeaderBackground: '#182444',
  SidebarBackground: '#0f1a34',
  SidebarItemActive: '#091023',
  SidebarFont: '#ffffff',
  PrimaryDarkest: '#0b0d0e',
  PrimaryDarker: '#171a1c',
  PrimaryRGB: '33, 37, 41',
  Primary: '#212529',
  Primary60: '#848587',
  SecondaryRGB: '16, 124, 124',
  Secondary: '#107c7c',
  Success: '#93d294',
  SuccessDark: '#284c29',
  Danger: '#e47373',
  DangerDark: '#822121',
  Info: '#75b9e6',
  InfoDark: '#2f4a5c',
  White: '#ffffff',
};
