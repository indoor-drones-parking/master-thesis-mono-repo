'use client';

import React, { useState } from 'react';
import { Header } from '../../server/Navigation/Header';
import { Sidebar } from '../../server/Navigation/Sidebar';
import { PageContainer } from '../../server/Container/Container';

type Props = {
  children: React.ReactNode;
};

function Navigation({ children }: Props) {
  const [isSidebarOpen, setIsSidebarOpen] = useState<boolean>(true);

  const onToggleSidebar = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };

  return (
    <React.Fragment>
      <Header
        isSidebarOpen={isSidebarOpen}
        onToggleSidebar={onToggleSidebar}
      ></Header>
      <Sidebar isSidebarOpen={isSidebarOpen} />
      <PageContainer isSidebarOpen={isSidebarOpen}>{children}</PageContainer>
    </React.Fragment>
  );
}

export default Navigation;
