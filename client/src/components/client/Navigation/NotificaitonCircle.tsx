'use client';
import { database } from '@/lib/firestore/config';
import { Deviation } from '@/lib/models/Devation';
import { ref, onValue, off } from 'firebase/database';
import React, { useEffect, useState } from 'react';

const Notificaiton = () => {
  const [deviations, setDevitations] = useState<Deviation[]>();
  useEffect(() => {
    const deviatonsRef = ref(database, `deviations`);

    const handleData = (snapshot: any) => {
      const data = snapshot.val();

      if (data) {
        setDevitations(data);
      }
    };

    // Set up Firebase listener
    const unsubscribe = onValue(deviatonsRef, handleData);

    // Clean-up function
    return () => {
      // Unsubscribe from the Firebase listener when component unmounts
      off(deviatonsRef, 'value', handleData);
    };
  }, []); // Empty dependency array to run effect only once

  return (
    <>
      {deviations ? (
        <div className="ml-10 flex h-8 w-8 items-center justify-center rounded-full bg-red-500">
          {Object.values(deviations).length}
        </div>
      ) : (
        <></>
      )}
    </>
  );
};

export default Notificaiton;
