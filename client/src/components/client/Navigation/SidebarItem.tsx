'use client';
import Link from 'next/link';
import React from 'react';
import { usePathname } from 'next/navigation';

type Props = {
  href: string;
  children: React.ReactNode;
  isChild?: boolean;
  setActiveIgnorePath?: string;
};

function SidebarItem({ href, children, isChild, setActiveIgnorePath }: Props) {
  const pathName = usePathname();

  let isActive = pathName.startsWith(href);

  if (setActiveIgnorePath && pathName.startsWith(setActiveIgnorePath)) {
    isActive = !pathName.startsWith(setActiveIgnorePath);
  }

  return (
    <Link href={href}>
      <ul
        className={`hover:bg-popover hover:text-primary relative mb-2 flex h-12 items-center justify-start gap-x-1 rounded-lg px-4 ${!isActive && 'text-white'} transition-all duration-200 ease-in-out ${isActive && 'bg-popover text-primary'} ${isChild && 'ml-4'}`}
      >
        {children}
      </ul>
    </Link>
  );
}

export default SidebarItem;
