'use client';
import { handleDelete } from '@/lib/actions';
import { database } from '@/lib/firestore/config';
import { ParkingSpotsServiceById } from '@/lib/firestore/services/ParkingSpotsServiceById';
import { Mission, MapperMission } from '@/lib/models/Mission';
import { ParkingSpot } from '@/lib/models/ParkingSpot';
import { CatchingPokemonSharp } from '@mui/icons-material';
import { child, onValue, ref, remove, set } from 'firebase/database';
import { useRouter } from 'next/navigation';

import React, { useEffect, useState } from 'react';
import {
  showErrorNotification,
  showSuccessNotification,
} from '../Notifications';
import { DroneService } from '@/lib/firestore/services/DroneService';
import { Drone } from '@/lib/models/Drone';

interface Props {
  missions: Mission[];
}
const MissionOverview = ({ missions }: Props) => {
  const router = useRouter();
  const [missionsLive, setMissionsLive] = useState<Mission[]>([]);
  const [parkingSpotsByMission, setParkingSpotsByMission] = useState<{
    [key: string]: ParkingSpot[] | undefined;
  }>({});
  const [dronesInMissionData, setDronesInMissionData] = useState<{
    [key: string]: Drone[];
  }>({});

  useEffect(() => {
    // Function to fetch parking spots for a mission
    const getParkingSpotsForMission = async (mission: Mission) => {
      if ((mission as MapperMission).parkingSpotsIds === undefined) return;
      const parkingSpotsIds = (mission as MapperMission).parkingSpotsIds;
      const parkingSpotsData =
        await ParkingSpotsServiceById.getParkingSpotsByIds(
          parkingSpotsIds,
          mission.parkingFloorId
        );

      return parkingSpotsData;
    };

    // Function to fetch parking spots for all missions
    const getAllParkingSpots = async () => {
      const parkingSpotsByMissionData: {
        [key: string]: ParkingSpot[] | undefined;
      } = {};
      for (const mission of missions) {
        if (parkingSpotsByMissionData != undefined) {
          const parkingSpotsData = await getParkingSpotsForMission(mission);
          parkingSpotsByMissionData[mission.id] = parkingSpotsData;
        }
      }
      setParkingSpotsByMission(parkingSpotsByMissionData);
    };

    getAllParkingSpots();
  }, [missions, missionsLive]); // Fetch parking spots whenever missions change

  useEffect(() => {
    // Function to fetch parking spots for a mission
    const getDronesForMission = async (mission: Mission) => {
      const dronesIds = (mission as MapperMission).droneIds;
      const dronesData = await DroneService.getDronesById(dronesIds);

      return dronesData;
    };

    // Function to fetch parking spots for all missions
    const getAllDrones = async () => {
      const dronesInMissionData: { [key: string]: Drone[] } = {};
      for (const mission of missions) {
        const droneData = await getDronesForMission(mission);
        dronesInMissionData[mission.id] = droneData;
      }
      setDronesInMissionData(dronesInMissionData);
    };

    getAllDrones();
  }, [missions, missionsLive]); // Fetch parking spots whenever missions change

  useEffect(() => {
    const missionRef = ref(database, `/missions`);
    const unSub = onValue(missionRef, (snapshot) => {
      const data = snapshot.val();
      if (data) {
        // Ensure data is not null or undefined
        const missionsArray: Mission[] = Object.values(data);
        setMissionsLive(missionsArray);
      } else {
        // If data is null or undefined, set empty array
        setMissionsLive([]);
      }
    });
    return () => unSub();
  }, []);

  const handleStopMission = (mission: Mission) => {
    if (mission.status === 'ACTIVE') {
      const missionStautsRef = ref(database, `missions/${mission.id}/status`);
      set(missionStautsRef, 'IDLE');
    }
  };

  const handleStartMission = (mission: Mission) => {
    if (mission.status === 'IDLE') {
      const missionStautsRef = ref(database, `missions/${mission.id}/status`);
      set(missionStautsRef, 'ACTIVE');
    }
  };
  const handleEditMission = (mission: Mission) => {
    router.push(`/missions/${mission.id}/edit`);
    // Redirect to edit page with missionId
  };

  const getElapsedTime = (date: string) => {
    const dateStarted = new Date(date);
    const dateNow = new Date();
    const differenceMs = Math.abs(dateNow.getTime() - dateStarted.getTime());

    // Convert milliseconds to hours and minutes
    const hours = Math.floor(differenceMs / (1000 * 60 * 60));
    const minutes = Math.floor((differenceMs % (1000 * 60 * 60)) / (1000 * 60));

    return `${hours} h and ${minutes} min`;
  };
  const handleDeleteMission = (mission: Mission) => {
    const missionRef = ref(database, `missions/${mission.id}`);
    try {
      remove(missionRef);
      DroneService.updateDroneStatuses(mission.droneIds);
      showSuccessNotification('Mission deleted');
      router.refresh();
    } catch (err) {
      showErrorNotification('Error while deleting mission');
    }
  };

  const parkingFloorNumber = (floorId: String) => {
    return floorId.charAt(floorId.length - 1);
  };

  // Function to group missions by parking floor ID
  const groupMissionsByFloor = (missions: Mission[]) => {
    const groupedMissions: { [key: string]: Mission[] } = {};

    missions.forEach((mission) => {
      const floorId = mission.parkingFloorId;
      if (!groupedMissions[floorId]) {
        groupedMissions[floorId] = [];
      }
      groupedMissions[floorId].push(mission);
    });

    return groupedMissions;
  };

  const groupedMissions = groupMissionsByFloor(missionsLive);

  return (
    <div className="container mx-auto px-4 py-8">
      <h1 className="mb-8 text-4xl font-bold">Mission Overview</h1>
      {/* Iterate over each parking floor */}
      {Object.entries(groupedMissions).map(([floorId, missions]) => (
        <div key={floorId} className="mb-8">
          <h2 className="text-2xl font-semibold">
            Floor {parkingFloorNumber(floorId)}
          </h2>
          <div className="grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3">
            {missions.map((mission, idx) => (
              <div
                key={idx * 2}
                className=" overflow-hidden rounded-lg bg-white shadow-md"
              >
                <div className="flex h-full flex-col justify-between p-6">
                  <h2 className="mb-2 text-xl font-semibold">
                    {mission.type === 'GUIDER'
                      ? 'Guider Mission'
                      : 'Mapper Mission'}
                  </h2>
                  <div className="group relative transform duration-300  hover:cursor-default">
                    <p className="hover: group relative mb-2 transform cursor-default text-gray-600 duration-300 ">
                      <b>Status:</b> {mission.status}{' '}
                    </p>
                    {mission.status === 'IDLE' ? (
                      <div className="absolute bottom-3/4 left-1/2 mb-2 w-max -translate-x-1/2 transform rounded bg-black px-2 py-1 text-xs text-white opacity-0 transition-opacity duration-300 group-hover:opacity-100">
                        The mission is not running.
                      </div>
                    ) : (
                      <div className="absolute bottom-3/4 left-1/2 mb-2 w-max -translate-x-1/2 transform rounded bg-black px-2 py-1 text-xs text-white opacity-0 transition-opacity duration-300 group-hover:opacity-100">
                        The mission is running.
                      </div>
                    )}
                  </div>
                  <p className="mb-2 text-gray-600">
                    <b>Floor:</b> {parkingFloorNumber(mission.parkingFloorId)}
                  </p>
                  <p className="mb-2 text-gray-600">
                    {' '}
                    <b>Drones:</b>
                    <ul className="flex flex-row gap-1">
                      {dronesInMissionData[mission.id]?.map(
                        (drone, droneIdx) => (
                          <li
                            className="rounded bg-gray-200 p-2"
                            key={droneIdx}
                          >
                            {drone.name}{' '}
                          </li>
                        )
                      )}
                    </ul>
                  </p>

                  {mission.type === 'GUIDER' ? (
                    <></>
                  ) : (
                    <p className="mb-2 text-gray-600">
                      {' '}
                      <b>Parking spot numbers:</b>
                      <ul className="flex flex-row gap-1">
                        {parkingSpotsByMission[mission.id]
                          ?.sort((a, b) => a.number - b.number) // Sort the parking spots by number in ascending order
                          .map((parkingSpot, spotIdx) => (
                            <li
                              className="rounded bg-gray-200 p-2"
                              key={spotIdx}
                            >
                              {parkingSpot.number}
                            </li>
                          ))}
                      </ul>
                    </p>
                  )}

                  <div className="flex w-full gap-2 ">
                    {mission.status === 'IDLE' ? (
                      <button
                        className=" w-full rounded bg-green-500 px-4 py-2 text-white hover:bg-green-600"
                        onClick={() => handleStartMission(mission)}
                      >
                        Start
                      </button>
                    ) : (
                      <button
                        className="w-full rounded bg-red-500 px-4 py-2 text-white hover:bg-red-600"
                        onClick={() => handleStopMission(mission)}
                      >
                        Stop
                      </button>
                    )}
                    <button
                      className="w-full rounded bg-blue-500 px-4 py-2 text-white hover:bg-blue-600"
                      onClick={() => handleEditMission(mission)}
                    >
                      Edit
                    </button>
                    <button
                      className="w-full rounded bg-red-500 px-4 py-2 text-white hover:bg-red-600"
                      onClick={() => handleDeleteMission(mission)}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      ))}
    </div>
  );
};

export default MissionOverview;
