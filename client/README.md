# Client

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

1. First fill out the .env file with configurations from MinIO and Firebase.

2. Run the following commands to run the development server:

```bash 
npm install

npm run dev
```
3. Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


 ##Dependencies

 * Node

 * npm or yarn


