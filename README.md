# Utilizing Drones for Enhanced Indoor Parking Management

This is a repository including all the code developed during the development of a prototype for the master thesis. The repository is divided into different folders, where each folder contains code for a specific part of the system. Each folder has its own README with instructions on how to run the code, and link to the repositories where some of the codes are taken from. 

* /client - Next.js Frontend Application
* /slot_vision - AI Image Analysis to Detect Deviations
* /autonomous_navigation - All Libraries and Methods Used to Achieve Autonomous Navigation

## Kafka and MinIO

To run Kafka and MinIO download docker and docker compose, and run "docker compose up" in the terminal. Remember to change the MinIO access keys. 