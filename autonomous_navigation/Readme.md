# Autonomous Navigation

This folder is a combination of several other open-source projects, to enable autonomous projects. All of the projects except for DistDepth and orb-slam3-stereo-fixed-master has been changed on, and that is the reason all the code is included in the repository, and not just the link. Anyway, a lot of the findings would not have been possible without the inspiration from the open-source community

## Links to the different repositories

* [DistDepth](https://github.com/facebookresearch/DistDepth)
* [orb_slam3_ros2_ws](https://github.com/simengronli/ORB_SLAM3_ROS2)
* [orb-slam3-stereo-fixed-master](https://github.com/zang09/ORB-SLAM3-STEREO-FIXED)
* [tentone-tello-ros2-main](https://github.com/tentone/tello-ros2)

## Run 

1. Install and build dependencies

2. Source all the packages as <package-name>/install/setup(.bash/.zsh) and add to your terminal config

3. Run to tentone-tello-ros2-main/scripts/run.sh

4. If you want to run orbslam, run "make orbslam" and if you want to run depth estimation run "make laserscan". To run nav2 run "make nav2"

## Dependencies 

* ROS 2

* OpenCV

* DistDepth (Install from folder in repository)

* ORB-SLAM3 (Install from folder in repository)