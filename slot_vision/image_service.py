from transformers import AutoImageProcessor, Mask2FormerForUniversalSegmentation
import torch
from enum import Enum

class ImageMapping(Enum):
    OCCUPIED = 1
    UNOCCUPIED = 2
    DEVIATION = 3

class ImageService:
    def __init__(self):
        self.model = Mask2FormerForUniversalSegmentation.from_pretrained("facebook/mask2former-swin-large-cityscapes-semantic")
        self.processor = AutoImageProcessor.from_pretrained("facebook/mask2former-swin-large-cityscapes-semantic")
        self.car_treshold = 20
        self.trailer_treshold = 10
        self.motorcycle_treshold = 5
        self.bus_treshold = 20

    def crop_image(self, img):
        original_width, original_height = img.size

        if original_width < 2592:
            return img

        # Define the amount to crop from each side
        crop_left = 500
        crop_right = 500

        area = (crop_left, 0, original_width - crop_right, original_height)
        return img.crop(area)

    def predict_image(self, image):
        cropped = self.crop_image(image)
        inputs = self.processor(images=cropped, return_tensors="pt")
        with torch.no_grad():
            outputs = self.model(**inputs)
        return self.processor.post_process_semantic_segmentation(outputs, target_sizes=[image.size[::-1]])[0]

    def calculate_pixel_percentage(self, prediction, total_pixels, class_id):
        elements_equal_to_class_id = torch.eq(prediction, class_id)
        count_of_class_id = torch.sum(elements_equal_to_class_id).item()
        return count_of_class_id * 100 / total_pixels


    def calulate_percentages(self, prediction):
        dict = {}
        total_pixels = prediction.numel()
        dict["car"] = self.calculate_pixel_percentage(prediction, total_pixels, 13)
        dict["motorcycle"] = self.calculate_pixel_percentage(prediction, total_pixels, 17)
        dict["trailer"] = self.calculate_pixel_percentage(prediction, total_pixels, 14)
        dict["bus"] = self.calculate_pixel_percentage(prediction, total_pixels, 15)
        return dict

    def evaluate_image(self, image):
        prediction = self.predict_image(image)
        calulations = self.calulate_percentages(prediction)

        if calulations["car"] > self.car_treshold:
            return ImageMapping.OCCUPIED
        elif calulations["trailer"] > self.trailer_treshold:
            return ImageMapping.DEVIATION, "Parked Truck or Trailer"
        elif calulations["bus"] > self.bus_treshold:
            return ImageMapping.DEVIATION, "Parked Bus"
        elif calulations["motorcycle"] > self.motorcycle_treshold:
            return ImageMapping.DEVIATION, "Parked motorcycle in car parking"
        return ImageMapping.UNOCCUPIED

