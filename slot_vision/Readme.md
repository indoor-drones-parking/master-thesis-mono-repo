# Deviation Detection

Each Folder Includes different approaches on doing image analysis on drone pictures from parking garages, including license plate detection and occupancy detection. The evaluations that is presented as results in the master thesis, is possible to find as a jupyter notebook in the LicensePlate folder and in the Mask2Former folder. 

To run the system presented in the prototype it is enough to run the image_service.py executable, it is necessary to connect it to a MinIO, Kafka and a Firestore Real Time Database, where everything can be done changing the image_service.py file.

## Dependencies

* Python 3
* pip