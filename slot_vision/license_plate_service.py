import yolov5
import cv2
import easyocr
import numpy as np
class LicensePlateService:
    def __init__(self):
        self.model = yolov5.load('keremberke/yolov5n-license-plate')
        self.model.conf = 0.25  # NMS confidence threshold
        self.model.iou = 0.45  # NMS IoU threshold
        self.model.agnostic = False  # NMS class-agnostic
        self.model.multi_label = False  # NMS multiple labels per box
        self.model.max_det = 1000  # maximum number of detections per image


    def predict_license_plate_number(self, pil_img):
        img = np.array(pil_img)
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        # Perform inference
        results = self.model(img, size=640)

        # Inference with test time augmentation
        results = self.model(img, augment=True)

        # Parse results
        predictions = results.pred[0]
        boxes = predictions[:, :4]  # x1, y1, x2, y2

        reader = easyocr.Reader(['en'])  # Initialize with English language

        # Variable to store the license plate number
        license_plate_number = None

        # Extract license plate regions and apply OCR
        for box in boxes:
            x1, y1, x2, y2 = map(int, box)  # Convert to int
            roi = img[y1:y2, x1:x2]  # Extract the region of interest (license plate)

            # Apply OCR to the ROI
            ocr_result = reader.readtext(roi)

            # Extract the license plate number from OCR results
            if ocr_result:
                license_plate_number = ocr_result[0][-2]
                break
        return license_plate_number
