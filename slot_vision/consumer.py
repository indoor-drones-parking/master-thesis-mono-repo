from kafka import KafkaConsumer
from minio import Minio
from minio.error import S3Error
from PIL import Image
from image_service import ImageService, ImageMapping
from license_plate_service import LicensePlateService
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import datetime
import uuid
from datetime import datetime
import io

def update_parking_spot(floor_id, parking_spot, evaluation):
    doc_ref = db.reference(f"parkingFloors/{floor_id}/{parking_spot}")
    if evaluation == ImageMapping.OCCUPIED:
        doc_ref.update({'isOccupied': True})
        doc_ref.update({"License_Plate": ""})
    if evaluation == ImageMapping.UNOCCUPIED:
        doc_ref.update({'isOccupied': False})
    print("Parking spot updated")


def update_occupied_parkingspot(floor_id, parking_spot):
    doc_ref = db.reference(f"parkingFloors/{floor_id}/{parking_spot}")
    doc_ref.update({'isOccupied': True, 'last_mapped': datetime.now().isoformat()})


def update_unoccupied_parkingspot(floor_id, parking_spot):
    doc_ref = db.reference(f"parkingFloors/{floor_id}/{parking_spot}")
    doc_ref.update({'isOccupied': False, 'licensePlate': "", 'last_mapped': datetime.now().isoformat()})


def create_deviation(image_path, description, minio_client):
    id = uuid.uuid4()
    id_string = str(id)
    parking_id = image_path.split('/')[1]

    copy_image_between_buckets(minio_client, "mapping-images", image_path, "deviation-images", id_string)

    doc_ref = db.reference(f"deviations/{id_string}")
    deviation_data = {
        'id': id_string,
        'description': description,
        'timestamp': datetime.now().isoformat(),
        'image': id_string,
        'parkingId': parking_id,
        'parkingNumber': id_to_number_mapping[parking_id]
    }
    doc_ref.set(deviation_data)


def create_over_free_time_deviation(image_path, description, first_mapped, minio_client):
    id = uuid.uuid4()
    id_string = str(id)
    parking_id = image_path.split('/')[1]

    copy_image_between_buckets(minio_client, "mapping-images", image_path, "deviation-images", id_string)
    copy_next_latest_version_image(minio_client, "mapping-images", image_path, "deviation-images", f"{id_string}_2")

    doc_ref = db.reference(f"deviations/{id_string}")
    deviation_data = {
        'id': id_string,
        'description': description,
        'timestamp': datetime.now().isoformat(),
        "firstMappedImage": f"{id_string}_2",
        "firstMappedTimestamp": first_mapped,
        'image': id_string,
        'parkingId': parking_id,
        'parkingNumber': id_to_number_mapping[parking_id]
    }
    doc_ref.set(deviation_data)


def get_image_as_pil(minio_client, bucket_name, object_name, showImage=False):
    try:
        data = minio_client.get_object(bucket_name, f"{object_name}.jpg")
        image_data = io.BytesIO()
        for d in data.stream(32 * 1024):
            image_data.write(d)
        image_data.seek(0)

        image = Image.open(image_data)
        if showImage:
            image.show()
        return image
    except S3Error as err:
        print("Error occurred: ", err)


def create_consumer(topic_name):
    consumer = KafkaConsumer(
        topic_name,
        bootstrap_servers=['amado:9092'],
        auto_offset_reset='earliest',
        group_id='my-group'
    )
    return consumer


def isOverFreeTimeCheck(new_license_plate, floor_id, parking_spot, minio_client):
    doc_ref = db.reference(f"parkingFloors/{floor_id}/{parking_spot}")
    if not doc_ref.get("hasFreePeriod"):
        return False

    parking_info = doc_ref.get()
    license_plate = ""

    try:
        license_plate = parking_info["licensePlate"]
    except:
        print("Failed to find the parkingSpots licensePlate")

    if license_plate == "":
        setFirstMapped(doc_ref, new_license_plate)
        return

    if new_license_plate == license_plate:
        free_time = parking_info["freeTimeDurationInSeconds"]
        first_mapped = parking_info["firstMapped"]
        if isOverParkingLimit(first_mapped, free_time):
            create_over_free_time_deviation(f"{floor_id}/{parking_spot}", "Parked Over Free Time Period", first_mapped,
                                            minio_client)
    else:
        setFirstMapped(doc_ref, new_license_plate)


def setFirstMapped(doc_ref, license_plate):
    doc_ref.update({'firstMapped': datetime.now().isoformat(), 'licensePlate': license_plate})


def isOverParkingLimit(first_mapped, duration):
    first_mapped_time = datetime.fromisoformat(first_mapped)
    current_time = datetime.now()
    time_difference = current_time - first_mapped_time
    return time_difference.total_seconds() > duration


def copy_image_between_buckets(minio_client, source_bucket, source_object, destination_bucket, destination_object):
    try:
        response = minio_client.get_object(source_bucket, f"{source_object}.jpg")
        size = int(response.headers.get('Content-Length'))
        minio_client.put_object(destination_bucket, destination_object, response, size,
                                content_type=response.headers.get('Content-Type'))

        print(
            f"Image copied from {source_bucket}/{source_object} to {destination_bucket}/{destination_object} successfully.")
    except S3Error as e:
        print(f"Error occurred: {e}")


def copy_next_latest_version_image(minio_client, source_bucket, source_object_name, dest_bucket, dest_object_name):
    try:
        objects = minio_client.list_objects_v2(source_bucket, prefix=source_object_name, recursive=True)

        image_objects = [obj for obj in objects if obj.object_name == source_object_name]
        sorted_images = sorted(image_objects, key=lambda obj: obj.last_modified, reverse=True)

        if len(sorted_images) < 2:
            print("Not enough image versions available to copy the next latest version.")
            return

        next_latest_image = sorted_images[1]

        minio_client.copy_object(
            dest_bucket,
            dest_object_name,
            f'/{source_bucket}/{next_latest_image.object_name}'
        )

        print(f"Successfully copied {next_latest_image.object_name} to {dest_object_name} in bucket {dest_bucket}")

    except S3Error as e:
        print(f"Error occurred: {e}")


def listen_to_topic(minio_client):
    consumer = create_consumer('parking_images')
    print("Listening to 'parking_images'...")

    image_service = ImageService()
    license_plate_service = LicensePlateService()

    for message in consumer:
        image_path = message.value.decode('utf-8')
        parts = image_path.split('/')

        image = get_image_as_pil(minio_client, "mapping-images", image_path)
        evaluation = image_service.evaluate_image(image)
        if evaluation != ImageMapping.UNOCCUPIED and evaluation != ImageMapping.OCCUPIED:
            create_deviation(image_path, evaluation[1], minio_client)
            print("Created Deviation")
        if evaluation == ImageMapping.OCCUPIED:
            license_plate_prediction = license_plate_service.predict_license_plate_number(image)
            license_plate = license_plate_prediction
            if license_plate_prediction is not None:
                license_plate = license_plate.replace(" ", "")
                isOverFreeTimeCheck(license_plate, parts[0], parts[1], minio_client)

            update_occupied_parkingspot(parts[0], parts[1])
            print("Updated ParkingSpot")
        if evaluation == ImageMapping.UNOCCUPIED:
            update_unoccupied_parkingspot(parts[0], parts[1])


if __name__ == '__main__':
    minio = Minio(
        "",
        access_key="",
        secret_key="",
        secure=False,
    )
    cred = credentials.Certificate('/key/serviceAccountkey.json')
    firebase_admin.initialize_app(cred, {
        'databaseURL': ''
    })
    listen_to_topic(minio)
